use std::io::{Error, BufRead, BufReader};
use std::fs::File;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>) -> i32 {
}

// fn solve_second(values: &Vec::<String>) -> i32 {
// }

fn main() {
    let values = load_values(String::from("input.txt"));
    let result_first = solve_first(&values);
    println!("{}", result_first);
    // assert_eq!(result_first, 0);
    // let result_second = solve_second(&values);
    // println!("{}", result_second);
    // assert_eq!(result_second, 0);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_first() {
        let values = load_values(String::from("sample.txt"));
        assert_eq!(solve_first(&values), 0);
    }

    // #[test]
    // fn test_solve_second() {
    //     let values = load_values(String::from("sample.txt"));
    //     assert_eq!(solve_second(&values), 0);
    // }
}

