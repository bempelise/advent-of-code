package main

import "bufio"
import "fmt"
import "log"
import "math"
import "os"
import "strconv"
import "strings"

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

type operation struct {
    value int
    numbers []int
}


func parse_operation(line string) operation {
    tokens := strings.Split(line, `:`)

    value, err := strconv.Atoi(tokens[0])

    if err != nil {
        panic(err)
    }

    numbers := []int{}

    for _, token := range strings.Fields(tokens[1]) {
        number, err := strconv.Atoi(token)

        if err != nil {
            panic(err)
        }

        numbers = append(numbers, number)
    }

    return operation { value: value, numbers: numbers }
}

func concat(a int, b int) int {
    tmp := b
    digits := 0

    for tmp > 0 {
        tmp = tmp/10
        digits++
    }

    return a * int(math.Pow(float64(10), float64(digits))) + b
}

func solve(op operation) bool {
    // fmt.Println(op)
    return solve_value(op.value, op.numbers[0], op.numbers[1:])
}

func solve2(op operation) bool {
    return solve2_value(op.value, op.numbers[0], op.numbers[1:])
}

func solve_value(target int, next int, numbers []int) bool {
    if len(numbers) < 2 {
        return target == numbers[0] * next || target == numbers[0] + next
    }
    return solve_value(target, next + numbers[0], numbers[1:]) ||
        solve_value(target, next * numbers[0], numbers[1:])
}

func solve2_value(target int, next int, numbers []int) bool {
    if len(numbers) < 2 {
        return target == numbers[0] * next || target == numbers[0] + next || target == concat(next, numbers[0])
    }
    return solve2_value(target, next + numbers[0], numbers[1:]) ||
        solve2_value(target, next * numbers[0], numbers[1:]) ||
        solve2_value(target, concat(next, numbers[0]), numbers[1:])
}

func part1(input []string) int {
    sum := 0

    for _, line := range input {
        op := parse_operation(line)

        if (solve(op)) {
            sum += op.value
        }
    }

    return sum
}

func part2(input []string) int {
    sum := 0

    for _, line := range input {
        op := parse_operation(line)

        if (solve2(op)) {
            sum += op.value
        }
    }

    return sum

}

func main() {
    want_part1_sample := 3749
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 6083020304036
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_sample := 11387
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 59002246504791
    part2_input := part2(parse_input("input.txt"))
    // fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_input))
    }
}

