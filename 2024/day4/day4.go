package main

import "bufio"
import "fmt"
import "log"
import "os"
// import "sort"
// import "strconv"
// import "strings"

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

type Position int

const (
    down_left Position = iota
    left
    up_left
    down
    up
    down_right
    right
    up_right
    none
)

type point struct {
    x int
    y int
    pos Position
}

func get_neighbours(width int, height int, i int, j int) []point {
    points := []point{}

    if i > 0 {
        if j > 0 {
            points = append(points, point{x: i - 1, y: j - 1, pos: down_left})
        }

        points = append(points, point{x: i - 1, y: j, pos: left})

        if j < height - 1 {
            points = append(points, point{x: i - 1, y: j + 1, pos: up_left})
        }
    }

    if j > 0 {
        points = append(points, point{x: i, y: j - 1, pos: down})
    }

    if j < height - 1 {
        points = append(points, point{x: i, y: j + 1, pos: up})
    }

    if i < width - 1 {
        if j > 0 {
            points = append(points, point{x: i + 1, y: j - 1, pos: down_right})
        }

        points = append(points, point{x: i + 1, y: j, pos: right})

        if j < height - 1 {
            points = append(points, point{x: i + 1, y: j + 1, pos: up_right})
        }
    }

    return points
}

func get_neighbour(width int, height int, i int, j int, pos Position) point {
    switch pos {
    case down_left:
        if i > 0 && j > 0 {
            return point{x: i - 1, y: j - 1, pos: down_left}
        }
    case left:
        if i > 0 {
            return point{x: i - 1, y: j, pos: left}
        }
    case up_left:
        if i > 0 && j < height - 1 {
            return point{x: i - 1, y: j + 1, pos: up_left}
        }
    case down:
        if j > 0 {
            return point{x: i, y: j - 1, pos: down}
        }
    case up:
        if j < height - 1 {
            return  point{x: i, y: j + 1, pos: up}
        }
    case down_right:
        if i < width - 1 &&  j > 0 {
            return point{x: i + 1, y: j - 1, pos: down_right}
        }
    case right:
        if i < width - 1 {
            return point{x: i + 1, y: j, pos: right}
        }
    case up_right:
        if i < width - 1 && j < height - 1 {
            return point{x: i + 1, y: j + 1, pos: up_right}
        }
    }
    return point{x: 0, y: 0, pos: none}
}

func part1(input []string) int {
    width := len(input[0])
    height := len(input)
    buffer := make([][]byte, height)

    for index, line := range input {
        buffer[index] = []byte(line)
    }

    count := 0

    for j := 0; j < height; j++ {
        for i := 0; i < width; i++ {
            if buffer[j][i] == 'X' {
                neighs := get_neighbours(width, height, i, j)
                for _, neigh := range neighs {
                    if buffer[neigh.y][neigh.x] == 'M' {
                        neigh2 := get_neighbour(width, height, neigh.x, neigh.y, neigh.pos)
                        if neigh2.pos != none && buffer[neigh2.y][neigh2.x] == 'A' {
                            neigh3 := get_neighbour(width, height, neigh2.x, neigh2.y, neigh2.pos)
                             if neigh3.pos != none && buffer[neigh3.y][neigh3.x] == 'S' {
                                count += 1
                            }
                        }
                    }
                }
            }
        }
    }

    return count
}

func part2(input []string) int {
    width := len(input[0])
    height := len(input)
    buffer := make([][]byte, height)

    for index, line := range input {
        buffer[index] = []byte(line)
    }

    count := 0

      for j := 0; j < height; j++ {
        for i := 0; i < width; i++ {
            if buffer[j][i] == 'A' {
                upleft := get_neighbour(width, height, i, j, up_left)
                upright := get_neighbour(width, height, i, j, up_right)
                downleft := get_neighbour(width, height, i, j, down_left)
                downright := get_neighbour(width, height, i, j, down_right)

                if upleft.pos != none &&
                   upright.pos != none &&
                   downleft.pos != none &&
                   downright.pos != none {
                    if buffer[upleft.y][upleft.x] == 'M' &&
                       buffer[upright.y][upright.x] == 'S' &&
                       buffer[downleft.y][downleft.x] == 'M' &&
                       buffer[downright.y][downright.x] == 'S' {
                        count += 1
                    } else if buffer[upleft.y][upleft.x] == 'M' &&
                       buffer[upright.y][upright.x] == 'M' &&
                       buffer[downleft.y][downleft.x] == 'S' &&
                       buffer[downright.y][downright.x] == 'S' {
                        count += 1
                    } else if buffer[upleft.y][upleft.x] == 'S' &&
                       buffer[upright.y][upright.x] == 'M' &&
                       buffer[downleft.y][downleft.x] == 'S' &&
                       buffer[downright.y][downright.x] == 'M' {
                        count += 1
                    } else if buffer[upleft.y][upleft.x] == 'S' &&
                       buffer[upright.y][upright.x] == 'S' &&
                       buffer[downleft.y][downleft.x] == 'M' &&
                       buffer[downright.y][downright.x] == 'M' {
                        count += 1
                    }
                }
            }
        }
    }

    return count

}

func main() {
    want_part1_sample := 18
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 2718
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_sample := 9
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 2046
    part2_input := part2(parse_input("input.txt"))
    // fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_input))
    }
}

