package main

import "bufio"
import "fmt"
import "log"
import "os"
import "sort"
import "strconv"
import "strings"

func abs_int(x int) int {
   if x < 0 {
    return -x
   }
   return x
}

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

func get_left_right(input []string)  ([]int, []int) {
    left := []int{}
    right := []int{}

    for _, line := range input {
        fields := strings.Fields(line)
        value, err := strconv.Atoi(fields[0])
        if err != nil {
            panic(err)
        }

        left = append(left, value)

        value, err = strconv.Atoi(fields[1])
        if err != nil {
            panic(err)
        }
        right = append(right, value)
    }

    sort.Ints(left)
    sort.Ints(right)

    return left, right
}

func part1(input []string) int {

    left, right := get_left_right(input)
    diff := 0

    for index, l := range left {
        r := right[index]
        diff += abs_int(l - r)
    }

    return diff
}

func part2(input []string) int {
    left, right := get_left_right(input)

    similarity := 0

    for _, lvalue := range left {
        mult := 0
        for _, rvalue := range right {
            if rvalue == lvalue {
                mult += 1
            }
        }

        similarity += lvalue * mult
    }

    return similarity
}

func main() {
    want_part1_sample := 11
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 1970720
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_sample := 31
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 17191599
    part2_input := part2(parse_input("input.txt"))
    // fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_sample))
    }
}

