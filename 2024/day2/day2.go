package main

import "bufio"
import "fmt"
import "log"
import "os"
import "strconv"
import "strings"

func stoi_array(input []string) []int {
	result := []int{}
	for _, value := range input {
		conv, err := strconv.Atoi(value)
		if err != nil {
		    panic(err)
		}

		result = append(result, conv)
	}

	return result
}

func array_filter(src []int, filter func(int, int) bool) []int {
    dest := []int{}
    for index, value := range src {
        if filter(index, value) {
            dest = append(dest, value)
        }
    }
    return dest
}

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

func check_reports(input []string) int {

	safe := 0

    for _, line := range input {
        report := stoi_array(strings.Fields(line))

        if check_report(report) {
        	safe += 1
        }
	}

	return safe
}

func check_reports_thoroughly(input []string) int {
	safe := 0

	for _, line := range input {
	    report := stoi_array(strings.Fields(line))

	    if check_report(report) {
	    	safe += 1
	    } else {
	    	for i := 0; i < len(report); i++ {
	    		if check_report(array_filter(report,  func(index int, value int) bool { return index != i })) {
	    			safe +=1
	    			break
	    		}
	    	}
	    }
	}

	return safe
}

func check_report(report []int) bool {
    has_ascend := false
    has_descend := false
    previous := 0
    previous_set := false
    unsafe := false

    for _, value := range report {
    	if !previous_set {
    		previous = value
    		previous_set = true
    		continue
    	}

    	if value > previous {
    		if has_descend {
    			unsafe = true
    			break
    		} else if value - previous > 3 {
    			unsafe = true
    			break
    		}
    		has_ascend = true
    	} else if value < previous {
    		if has_ascend {

    			unsafe = true
    			break
    		} else if previous - value > 3 {
    			unsafe = true
    			break
    		}
    		has_descend = true
    	} else {
    		unsafe = true
    		break
    	}

		previous = value
    }

    // if (!unsafe) {
    // 	fmt.Println(report)
    // }

    return !unsafe
}


func part1(input []string) int {
    return check_reports(input)
}

func part2(input []string) int {
    return check_reports_thoroughly(input)
}

func main() {
    want_part1_sample := 2
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 202
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_sample := 4
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 271
    part2_input := part2(parse_input("input.txt"))
    // fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_sample))
    }
}

