package main

import "bufio"
import "fmt"
import "log"
import "os"
import "strconv"
import "strings"
import "slices"

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

func parse_rule(line string) (int, int) {
    tokens := strings.Split(line, "|")

    left, err := strconv.Atoi(tokens[0])

    if err != nil {
        panic(err)
    }

    right, err := strconv.Atoi(tokens[1])

    if err != nil {
        panic(err)
    }

    return left, right
}

func parse_update(line string) []int {
    update := []int{}
    tokens := strings.Split(line, ",")

    for _, token := range tokens {
        value, err := strconv.Atoi(token)

        if err != nil {
            panic(err)
        }

        update = append(update, value)
    }

    return update
}

func parse_rules_updates(input []string) (map[int][]int, [][]int){
    rules := make(map[int][]int)
    updates := [][]int{}

    parsed_rules := false

    for _, line := range input {
        if line == "" {
            parsed_rules = true
            continue
        }

        if parsed_rules {
            updates = append(updates, parse_update(line))
        } else {
            left, right := parse_rule(line)

            _, present := rules[left]

            if !present {
                rules[left] = []int{ right }
            } else {
                rules[left] = append(rules[left], right)
            }
        }
    }

    return rules, updates
}

func validate_update(rules map[int][]int, update []int) bool {
    for i, value := range update {
        descendants := rules[value]
        for j := 0; j < i; j++ {
            if slices.Contains(descendants, update[j]) {
                return false
            }
        }
    }
    return true
}

func get_middle(update []int) int {
    return update[(len(update))/2]
}

func part1(input []string) int {
    sum := 0
    rules, updates := parse_rules_updates(input)
    // fmt.Println(rules)
    // fmt.Println(updates)

    for _, update := range updates {
        valid := validate_update(rules, update)
        if valid {
            // fmt.Println(update)
            sum += get_middle(update)
        }
    }

    return sum
}

func fix_then_get_middle(rules map[int][]int, update []int) int {
    for i, value := range update {
        descendants := rules[value]
        for j := 0; j < i; j++ {
            if slices.Contains(descendants, update[j]) {
                temp := update[j]
                update[j] = update[i]
                update[i] = temp
            }
        }
    }

    return update[(len(update))/2]
}

func part2(input []string) int {
    sum := 0
    rules, updates := parse_rules_updates(input)
    for _, update := range updates {
        valid := validate_update(rules, update)
        if !valid {
            sum += fix_then_get_middle(rules, update)
        }
    }
    return sum

}

func main() {
    want_part1_sample := 143
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 5713
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_sample := 123
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 5180
    part2_input := part2(parse_input("input.txt"))
    // fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_input))
    }
}

