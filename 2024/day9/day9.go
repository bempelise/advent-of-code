package main

import "bufio"
import "fmt"
import "log"
import "os"
import "strconv"
import "strings"

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

func print_mem(memmap []string) {
    fmt.Println(strings.Join(memmap, ""))
}

func get_memmap(line string) []string {
    memmap := []string{}
    space := false
    file_id := 0

    for _, c := range line {
        value := int(c - '0')
        if space {
            // fmt.Println("space ", value)
            for i := 0; i < value; i++ {
                memmap = append(memmap, ".")
            }
            space = false
        } else {
            // fmt.Println("not space ", c)
            for i := 0; i < value; i++ {
                memmap = append(memmap, strconv.Itoa(file_id))
            }
            file_id++
            space = true
        }
    }

    return memmap
}

func defrag(memmap []string) {
    i := 0
    j := len(memmap) - 1

    for i < j {
        if memmap[i] != "."  {
            i++
            continue
        }

        if memmap[j] == "." {
            j--
            continue
        }

        memmap[i] = memmap[j]
        memmap[j] = "."
        i++
        j--
    }
}

type entry struct {
    start int
    size int
    id string
}

func print_entry(e entry) {
    fmt.Println("St: ", e.start, " Si: ", e.size, " id: ", e.id)
}

func get_files(memmap []string) []entry {
    count := 0
    position := 0
    entries := []entry{}
    id := ""

    for i := 0; i < len(memmap); i++ {
        if memmap[i] != "." {

            if count == 0 {
                position = i
                id = memmap[i]
            } else if memmap[i] != id {
                entries = append(entries, entry{start: position, size: count, id: id})
                count = 0
                position = i
                id = memmap[i]
            }

            count++;
        } else if count != 0 {
            entries = append(entries, entry{start: position, size: count, id: id})
            position = 0
            count = 0
        }
    }

    if count != 0 {
        entries = append(entries, entry{start: position, size: count, id: id})
    }

    return entries
}

func fillin(memmap []string) {
    files := get_files(memmap)

    // fmt.Println(files)

    print_mem(memmap)

    for j := len(files) - 1; j >= 0; j-- {
        print_entry(files[j])
        for i := 0; i < files[j].start; {
            if memmap[i] != "." {
                i++
                continue
            } else {
                gapsize := 0
                for k := i; k < len(memmap) && memmap[k] == "."; k++ {
                    gapsize++
                }

                if gapsize >= files[j].size {
                    for k := 0; k < files[j].size; k++ {
                        memmap[i + k] = memmap[files[j].start + k]
                        memmap[files[j].start + k] = "."
                    }
                }

                i += gapsize

                // print_mem(memmap)
            }
        }
    }
}

func checksum(memmap []string) int {
    sum := 0

    for i, value_str := range memmap {
        if value_str == "." {
            continue
        }

        value, err := strconv.Atoi(value_str)
        if err != nil {
            panic(err)
        }
        sum += i * value
    }

    return sum
}


func part1(input []string) int {
    memmap := get_memmap(input[0])
    defrag(memmap)
    return checksum(memmap)
}

func part2(input []string) int {
    memmap := get_memmap(input[0])
    fillin(memmap)
    return checksum(memmap)
}

func main() {
    want_part1_sample := 1928
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 6461289671426
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_sample := 2858
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 6488291456470
    part2_input := part2(parse_input("input.txt"))
    fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_input))
    }
}

