package main

import "bufio"
import "fmt"
import "log"
import "os"
import "strconv"
import "strings"
import "regexp"

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

func get_mult(mul string) int {
    // fmt.Println(mul)
    mul = strings.Replace(mul, "mul(", "", -1)
    mul = strings.Replace(mul, ")", "", -1)
    operands := strings.Split(mul, ",")

    left, err := strconv.Atoi(operands[0])
    if err != nil {
        panic(err)
    }

    right, err := strconv.Atoi(operands[1])
    if err != nil {
        panic(err)
    }

    return left * right
}

func get_do(do string) bool {
    // fmt.Println(do)
    if do == "do()" {
        return true
    } else if do == "don't()" {
        return false
    }
    log.Fatal(fmt.Sprintf("Unknown do command: %v", do))
    return false
}

func part1(input []string) int {
    sum := 0
    re := regexp.MustCompile(`mul\(\d*,\d*\)`)

    for _, line := range input {
        matches := re.FindAllString(line, -1)

        if matches == nil {
            continue
        }

        for _, match := range matches {
            sum += get_mult(match)
        }
    }
    return sum
}

func part2(input []string) int {
    sum := 0

    re_mul := regexp.MustCompile(`mul\(\d*,\d*\)`)
    re_dos := regexp.MustCompile(`do\(\)|don't\(\)`)
    do := true

    for _, line := range input {
        mul := re_mul.FindAllStringIndex(line, -1)
        // fmt.Println(mul)
        dos := re_dos.FindAllStringIndex(line, -1)
        // fmt.Println(dos)

        ix_mul := 0
        ix_dos := 0

        for ix_mul < len(mul) {
            // if ix_dos < len(dos) {
            //     fmt.Println(do, ":", line[mul[ix_mul][0]:mul[ix_mul][1]], ",", line[dos[ix_dos][0]:dos[ix_dos][1]])
            // }

            if ix_dos < len(dos) && dos[ix_dos][0] < mul[ix_mul][0] {
                // fmt.Println("DO: ", dos[ix_dos][0])
                do = get_do(line[dos[ix_dos][0]:dos[ix_dos][1]])
                ix_dos += 1
            } else if ix_dos >= len(dos) ||  ix_mul < len(mul) && mul[ix_mul][0] < dos[ix_dos][0] {
                // fmt.Println("MUL: ", mul[ix_mul][0])
                if do {
                    sum += get_mult(line[mul[ix_mul][0]:mul[ix_mul][1]])
                }
                ix_mul += 1
            }
        }

    }

    return sum

}

func main() {
    want_part1_sample := 161
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 175615763
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_sample := 48
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 74361272
    part2_input := part2(parse_input("input.txt"))
    // fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_input))
    }
}

