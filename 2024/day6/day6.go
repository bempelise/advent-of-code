package main

import "bufio"
import "fmt"
import "log"
import "os"
// import "sort"
// import "strconv"
// import "strings"

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

type Direction int

const (
    up Direction = iota
    down
    left
    right
    none
)

type Position struct {
    x int
    y int
}

type Visit struct {
    pos Position
    dir Direction
}

type Area struct {
    width int
    height int
    pos Position
    dir Direction
    amap [][]byte
}

func copy_area(o Area) Area {
    parea := Area {
        width: o.width,
        height: o.height,
        pos: o.pos,
        dir: up,
        amap: make([][]byte, o.height),
    }

    for j, line := range o.amap {
        parea.amap[j] = make([]byte, o.width)
        for i, c := range line {
            parea.amap[j][i] = c
        }
    }

    return parea
}


func parse_area(input []string) Area     {
    width := len(input[0])
    height := len(input)

    parea := Area { width: width, height: height, pos: Position{x: 0, y: 0}, dir: up, amap: make([][]byte, height) }

    for j, line := range input {
        parea.amap[j] = make([]byte, width)
        for i, c := range []byte(line) {
            if c == '^' {
                parea.amap[j][i] = '.'
                parea.pos.x = i
                parea.pos.y = j
            } else {
                parea.amap[j][i] = c
            }
        }
    }

    return parea
}

func next_out_of_bounds(parea Area, pos Position) bool {
    return pos.x < 0 || pos.x >= parea.width || pos.y < 0 || pos.y >= parea.height
}

func next_blocked(parea Area, pos Position) bool {
    return parea.amap[pos.y][pos.x] == '#'
}

func rotate(dir Direction) Direction {
    switch dir {
    case up:
        return right
    case right:
        return down
    case down:
        return left
    case left:
        return up
    }
    return none
}

func next(pos Position, dir Direction) Position {
    switch dir {
    case up:
        return Position{x: pos.x, y: pos.y - 1}
    case down:
        return Position{x: pos.x, y: pos.y + 1}
    case left:
        return Position{x: pos.x - 1, y: pos.y}
    case right:
        return Position{x: pos.x + 1, y: pos.y}
    }

    return pos
}


func simulate(parea Area) int {
    moves := make(map[Position]struct{})
    pos := parea.pos
    dir := parea.dir

    for true {
        moves[pos] = struct{}{}
        next_pos := next(pos, dir)
        if next_out_of_bounds(parea, next_pos) {
            break
        } else if next_blocked(parea, next_pos) {
            dir = rotate(dir)
        } else {
            pos = next_pos
        }
    }
    return len(moves)
}

func part1(input []string) int {
    parea := parse_area(input)
    return simulate(parea)
}


func simulate_for_loops(parea Area) int {
    moves := make(map[Visit]struct{})
    pos := parea.pos
    dir := parea.dir

    obstacles := make(map[Position]struct{})
    moves[Visit{pos: pos, dir: dir}] = struct{}{}

    for true {
        next_pos := next(pos, dir)

        if next_out_of_bounds(parea, next_pos) {
            break
        } else if next_blocked(parea, next_pos) {
            dir = rotate(dir)
        } else {

            _, found := obstacles[next_pos]
            if !found {
            	obstacles[simulate_obstacle(parea, parea.pos, next_pos, parea.dir)] = struct{}{}
            }

            pos = next_pos
            moves[Visit{pos: pos, dir: dir}] = struct{}{}
        }
    }

    return len(obstacles) - 1
}


func simulate_obstacle(parea Area, pos Position, obstacle Position, dir Direction) Position {
    pcopy := copy_area(parea)
    visited :=  make(map[Visit]struct{})

	pcopy.amap[obstacle.y][obstacle.x] = '#'

	for true {
	    next_pos := next(pos, dir)
	    if next_out_of_bounds(pcopy, next_pos) {
	        break
	    } else if next_blocked(pcopy, next_pos) {
	        dir = rotate(dir)
	    } else {
	        pos = next_pos

            _, found := visited[Visit{pos: pos, dir: dir}]
            if found {
                return obstacle
            }

            visited[Visit{pos: pos, dir: dir}] = struct{}{}
	    }
	}

	return Position{x: -1, y: -1}
}

func part2(input []string) int {
	parea := parse_area(input)

    return simulate_for_loops(parea)
}

func main() {
    want_part1_sample := 41
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 4819
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_sample := 6
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 1796
    part2_input := part2(parse_input("input.txt"))
    // fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_input))
    }
}

