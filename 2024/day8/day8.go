package main

import "bufio"
import "fmt"
import "log"
import "os"
// import "sort"
// import "strconv"
// import "strings"

func parse_input(filename string) []string {
    f, err := os.Open(filename)

     if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    data := []string{}

    for scanner.Scan() {
        data = append(data, scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    return data
}

type position struct {
    x int
    y int
}

type dimension struct {
    width int
    height int
}

type antenna_map map[byte]map[position]struct{}

func (a antenna_map) add(freq byte, pos position) {

    set, ok := a[freq]

    if ok {
        set[pos] = struct{}{}
    } else {
        a[freq] = make(map[position]struct{})
        a[freq][pos] = struct{}{}
    }
}

func (a antenna_map) print() {

    for k, v := range a {
        fmt.Println(string(k), ": ", v)
    }
}

func read_antennas(input []string) antenna_map {
    antennas := make(antenna_map)

    for j, line := range input {
        for i, c := range []byte(line) {
            if c == '.' {
                continue
            }
            antennas.add(c, position{x: i, y: j})
        }
    }

    return antennas
}

func get_dimensions(input []string) dimension {
    return dimension{width: len(input[0]), height: len(input)}
}

func within_dimensions(pos position, dim dimension) bool {
    return pos.x >= 0 && pos.y >= 0 && pos.x < dim.width && pos.y < dim.height
}

func get_step(pos position, other position) position {
    return position {x: other.x - pos.x, y: other.y - pos.y }
}

func get_antinodes(freq byte, positions map[position]struct{}) map[position]struct{} {
    antinodes := make(map[position]struct{})

    for pos, _ := range positions {

        for other, _ := range positions {
            if pos == other {
                continue
            }

            step := get_step(pos, other)

            antinodes[position{x: pos.x - step.x, y: pos.y - step.y}] = struct{}{}
            antinodes[position{x: other.x + step.x, y: other.y + step.y}] = struct{}{}
        }
    }

    return antinodes
}

func get_all_antinodes(freq byte, positions map[position]struct{}, dim dimension) map[position]struct{} {
    antinodes := make(map[position]struct{})

    for pos, _ := range positions {

        for other, _ := range positions {
            if pos == other {
                continue
            }

            step := get_step(pos, other)

            next := pos

            for within_dimensions(next, dim) {
                antinodes[next] = struct{}{}
                next = position{x: next.x - step.x, y: next.y - step.y}
            }

            next = other

            for within_dimensions(next, dim) {
                antinodes[next] = struct{}{}
                next = position{x: next.x + step.x, y: next.y + step.y}
            }
        }
    }

    return antinodes
}


func part1(input []string) int {
    antennas := read_antennas(input)
    antinodes := make(map[position]struct{})
    dim := get_dimensions(input)

    for freq, positions := range antennas {
        new_antinodes := get_antinodes(freq, positions)

        for pos, _ := range new_antinodes {
            if within_dimensions(pos, dim) {
                antinodes[pos] = struct{}{}
            }
        }
    }

    return len(antinodes)
}

func part2(input []string) int {
    antennas := read_antennas(input)
    antinodes := make(map[position]struct{})
    dim := get_dimensions(input)

    for freq, positions := range antennas {
        new_antinodes := get_all_antinodes(freq, positions, dim)

        for pos, _ := range new_antinodes {
            if within_dimensions(pos, dim) {
                antinodes[pos] = struct{}{}
            }
        }
    }

    return len(antinodes)

}

func main() {

    want_part1_sample := 14
    part1_sample := part1(parse_input("sample.txt"))
    if part1_sample != want_part1_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_sample, want_part1_sample))
    }

    want_part1_input := 396
    part1_input := part1(parse_input("input.txt"))
    // fmt.Println(part1_input)
    if part1_input != want_part1_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part1_input, want_part1_input))
    }

    want_part2_tiny := 9
    part2_tiny := part2(parse_input("tiny.txt"))
    if part2_tiny != want_part2_tiny {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_tiny, want_part2_tiny))
    }

    want_part2_sample := 34
    part2_sample := part2(parse_input("sample.txt"))
    if part2_sample != want_part2_sample {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_sample, want_part2_sample))
    }

    want_part2_input := 1200
    part2_input := part2(parse_input("input.txt"))
    // fmt.Println(part2_input)
    if part2_input != want_part2_input {
        log.Fatal(fmt.Sprintf("Sample Part 1 failed (Got %v Expected %v)", part2_input, want_part2_input))
    }
}

