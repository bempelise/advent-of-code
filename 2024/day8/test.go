package main

import (
    "fmt"
)

type position struct {
    x int
    y int
}

func add(pos position, antennas map[byte]map[position]struct{}) {
    set, ok := antennas['q']

    if ok {
    	set[pos] = struct{}{}
    } else {
        antennas['q'] = make(map[position]struct{})
	    antennas['q'][pos] = struct{}{}
    }
}

func main() {
    antennas := make(map[byte]map[position]struct{})

    add(position{x: 1, y: 1}, antennas)
    add(position{x: 1, y: 2}, antennas)
    add(position{x: 2, y: 1}, antennas)
    add(position{x: 4, y: 1}, antennas)
    add(position{x: 1, y: 1}, antennas)


    fmt.Println(antennas)
}
