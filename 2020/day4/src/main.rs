use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::collections::HashMap;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn lines_to_passports(lines: &Vec::<String>) -> Vec::<HashMap<String, String>> {
    let mut passports = Vec::<HashMap<String, String>>::new();

    passports.push(HashMap::new());

    for line in lines {
        if line == "" {
            passports.push(HashMap::new());
        }
        else {
            let parts : Vec::<&str> = line.split_whitespace().collect();
            let lastn = passports.len() - 1;
            for part in parts {
                let kvp: Vec::<&str> = part.split(':').collect();
                let _ = &passports[lastn].insert(String::from(kvp[0]), String::from(kvp[1]));
            }
        }
    }

    return passports
}


fn solve_first(passports: &Vec::<HashMap<String, String>>, expected: i32) {
    let mut valid = 0;

    for passport in passports {
        match passport.get("cid") {
            Some(_) => {
                if passport.len() >= 8 {
                    valid += 1;
                }
            },
            None => {
                if passport.len() >= 7 {
                    valid += 1;
                }
            }
        }
    }

    println!("{}", valid);
    assert_eq!(expected, valid);
}

fn solve_second(passports: &Vec::<HashMap<String, String>>, expected: i32) {
    let mut valid = 0;

    let fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];


    for passport in passports {
        let mut is_valid = true;

        for field in fields {
            match passport.get(field) {
                Some(value) => {
                    match field {
                        "byr" => {
                            match value.parse::<i32>() {
                                Ok(year) => {
                                    if year < 1920 || year > 2002 {
                                        is_valid = false;
                                        println!("byr out of range");
                                    }
                                },
                                Err(_) => {
                                    is_valid = false;
                                    println!("byr bad value");
                                }
                            }
                        },
                        "iyr" => {
                            match value.parse::<i32>() {
                                Ok(year) => {
                                    if year < 2010 || year > 2020 {
                                        is_valid = false;
                                        println!("iyr out of range");
                                    }
                                },
                                Err(_) => {
                                    is_valid = false;
                                    println!("iyr bad value");
                                }
                            }
                        },
                        "eyr" => {
                            match value.parse::<i32>() {
                                Ok(year) => {
                                    if year < 2020 || year > 2030 {
                                        is_valid = false;
                                        println!("eyr out of range");
                                    }
                                },
                                Err(_) => {
                                    is_valid = false;
                                    println!("eyr bad value");
                                }
                            }
                        },
                        "hgt" => {
                            let len = value.len();

                            match &value[len-2..] {
                                "in" => {
                                    let height_opt = &value[..len-2].parse::<i32>();
                                    match height_opt {
                                        Ok(height) => {
                                            if *height < 59 || *height > 76
                                            {
                                                is_valid = false;
                                                println!("hgt (in) out of range");
                                            }
                                        },
                                        Err(_) => {
                                            is_valid = false;
                                            println!("hgt (in) bad value");
                                        }
                                    }
                                }
                                "cm" => {
                                    let height_opt = &value[..len-2].parse::<i32>();
                                    match height_opt {
                                        Ok(height) => {
                                            if *height < 150 || *height > 193
                                            {
                                                is_valid = false;
                                                println!("hgt (cm) out of range");
                                            }
                                        },
                                        Err(_) => {
                                            is_valid = false;
                                            println!("hgt (cm) bad value");
                                        }
                                    }
                                }
                                _ => {
                                    is_valid = false;
                                    println!("hgt bad metrics");
                                }
                            }
                        },
                        "hcl" => {
                            if value.len() != 7 {
                                println!("hcl wrong length");
                                is_valid = false;
                            }
                            else {
                                let mut count = 0;
                                for c in value.chars() {
                                    if count == 0 {
                                        if c != '#' {
                                            is_valid = false;
                                            println!("hcl not starting with #");
                                            break;
                                        }
                                    }
                                    else {
                                        match c {
                                            '0'..='9' | 'a'..='f' => (),
                                            _ => {
                                                is_valid = false;
                                                println!("hcl bad character: {} ", c);
                                            }
                                        }
                                    }

                                    count += 1;
                                }
                            }
                        },
                        "ecl" => {
                            match value.as_str() {
                                "amb" => (),
                                "blu" => (),
                                "brn" => (),
                                "gry" => (),
                                "grn" => (),
                                "hzl" => (),
                                "oth" => (),
                                _ => {
                                    is_valid = false;
                                    println!("ecl wrong value");
                                }
                            }
                        },
                        "pid" => {
                            if value.len() != 9 {
                                is_valid = false;
                                println!("pid wrong length");
                            }
                            else {
                                match value.parse::<u64>() {
                                    Ok(_) => (),
                                    Err(_) => {
                                        is_valid = false;
                                        println!("pid bad value");
                                    }
                                }
                            }
                        },
                        _ => ()
                    }
                },
                None => {
                    is_valid = false;
                    println!("{} missing", field);
                }
            }

            if !is_valid {
                break;
            }
        }

        if is_valid {
            valid += 1;
        }
    }

    println!("{}", valid);
    assert_eq!(expected, valid);
}

fn main() {
    let do_sample: bool = false;
    if do_sample {
        let values = lines_to_passports(&load_values(String::from("sample.txt")));
        let expected_first = 2;
        solve_first(&values, expected_first);

        let values_second = lines_to_passports(&load_values(String::from("sample2.txt")));
        let expected_second = 4;
        solve_second(&values_second, expected_second);
    } else {
        let values = lines_to_passports(&load_values(String::from("input.txt")));
        let expected_first = 206;
        let expected_second = 123;
        solve_first(&values, expected_first);
        solve_second(&values, expected_second);
    }
}
