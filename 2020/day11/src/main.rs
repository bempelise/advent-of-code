use std::io::{Error, BufRead, BufReader};
use std::fs::File;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>) -> i32 {
    let final_state = simulate(&values);
    let occupied_seats = get_occupied(&final_state);
    println!("{}", occupied_seats);
    return occupied_seats;
}

fn get_region(lines: &Vec::<String>, i: usize, j: usize) -> [[char; 3]; 3] {
    let mut region: [[char; 3]; 3] =  [['.'; 3]; 3];

    if i > 0 {
        if j > 0 {
            region[0][0] = lines[i-1].chars().nth(j-1).unwrap();
        }

        region[0][1] = lines[i-1].chars().nth(j).unwrap();

        if j < lines[i-1].len() - 1 {
            region[0][2] = lines[i-1].chars().nth(j+1).unwrap();
        }
    }

    if j > 0 {
        region[1][0] = lines[i].chars().nth(j-1).unwrap();
    }

    region[1][1] = lines[i].chars().nth(j).unwrap();

    if j < lines[i].len() - 1 {
        region[1][2] = lines[i].chars().nth(j+1).unwrap();
    }


    if i < lines.len() - 1 {
        if j > 0 {
            region[2][0] = lines[i+1].chars().nth(j-1).unwrap();
        }

        region[2][1] = lines[i+1].chars().nth(j).unwrap();

        if j < lines[i+1].len() - 1 {
            region[2][2] = lines[i+1].chars().nth(j+1).unwrap();
        }
    }

    // if i == 0 {
    //     println!("({}, {})", i, j);
    //     print_region(&region);
    // }

    return region;
}

fn get_entry(lines: &Vec::<String>, i: usize, j: usize) -> char {
    return lines[i].chars().nth(j).unwrap();
}

// fn print_region(region: &[[char; 3]; 3]) {
//     for i in 0..3 {
//         let mut line = String::new();
//         for j in 0..3 {
//             line.push(region[i][j]);
//         }
//         println!("{}", line);
//     }
// }

fn toggle(region: &[[char; 3]; 3]) -> bool {
    match region[1][1] {
        'L' => {
            for i in 0..3 {
                for j in 0..3 {
                    if region[i][j] == '#' {
                        return false;
                    }
                }
            }
            return true;
        },
        '#' => {
            let mut count = 0;
            for i in 0..3 {
                for j in 0..3 {
                    if i == j && i == 1 {
                        continue;
                    }

                    if region[i][j] == '#' {
                        count += 1;
                        if count >= 4 {
                            return true;
                        }
                    }
                }
            }
            return false;
        },
        _ => return false,
    }
}

fn simulate(values: &Vec::<String>) -> Vec::<String> {
    let mut state = values.to_vec();
    let mut next_state = Vec::<String>::new();

    loop {
        next_state.clear();
        let mut changed_state: bool = false;

        for i in 0..state.len() {
            let line = &state[i];
            let mut new_line = String::new();

            for j in 0..line.len() {
                let current: char = line.chars().nth(j).unwrap();
                match current {
                    'L' | '#' => {
                        if toggle(&get_region(&state, i , j)) {
                            if current == '#' {
                                new_line.push('L');
                            } else {
                                new_line.push('#');
                            }
                            changed_state = true;
                        } else {
                            new_line.push(current);
                        }
                    },
                    _ => new_line.push('.')
                }
            }

            next_state.push(new_line);
        }

        state = next_state.to_vec();

        // print_state(&state);

        if !changed_state {
            break;
        }
    }

    return state;
}

fn get_occupied(state: &Vec::<String>) ->  i32 {
    let mut count = 0;
    for line in state {
        for c in line.chars() {
            if c == '#' {
                count += 1;
            }
        }
    }

    return count
}

// fn print_state(state: &Vec::<String>) {
//     println!("");
//     for line in state {
//         println!("{}", line);
//     }
//     println!("");
// }

fn solve_second(values: &Vec::<String>) -> i32 {
    let final_state = simulate2(&values);
    let occupied_seats = get_occupied(&final_state);
    println!("{}", occupied_seats);
    return occupied_seats;
}

fn get_region2(lines: &Vec::<String>, i: usize, j: usize) -> [[char; 3]; 3] {
    let mut region: [[char; 3]; 3] =  [['.'; 3]; 3];

    let mut x;
    let mut y;

    if i > 0 {

        if j > 0 {
            x = i - 1;
            y = j - 1;

            loop {
                let entry = get_entry(&lines, x, y);
                if entry != '.' {
                    region[0][0] = entry;
                    break;
                }

                if x == 0 || y == 0 {
                    break;
                }

                x -= 1;
                y -= 1;
            }
        }

        x = i - 1;
        y = j;

        loop {
            let entry = get_entry(&lines, x, y);
            if entry != '.' {
                region[0][1] = entry;
                break;
            }

            if x == 0 {
                break;
            }

            x -= 1;
        }

        if j < lines[i].len() - 1 {
            x = i - 1;
            y = j + 1;

            loop {
                let entry = get_entry(&lines, x, y);
                if entry != '.' {
                    region[0][2] = entry;
                    break;
                }

                if x == 0 || y == lines[x].len() - 1 {
                    break;
                }

                x -= 1;
                y += 1;
            }
        }
    }

    x = i;

    if j > 0 {
        y = j - 1;

        loop {
            let entry = get_entry(&lines, x, y);
            if entry != '.' {
                region[1][0] = entry;
                break;
            }

            if y == 0 {
                break;
            }

            y -= 1;
        }
    }

    y = j;
    region[1][1] = get_entry(&lines, x, y);

    if j < lines[i].len() - 1 {
        y = j + 1;

        loop {
            let entry = get_entry(&lines, x, y);
            if entry != '.' {
                region[1][2] = entry;
                break;
            }

            if y == lines[x].len()- 1 {
                break;
            }

            y += 1;
        }
    }

    if i < lines.len() - 1 {
        if j > 0 {
            x = i + 1;
            y = j - 1;

            loop {
                let entry = get_entry(&lines, x, y);
                if entry != '.' {
                    region[2][0] = entry;
                    break;
                }

                if x == lines.len() - 1 || y == 0 {
                    break;
                }

                x += 1;
                y -= 1;
            }
        }

        x = i + 1;
        y = j;

        loop {
            let entry = get_entry(&lines, x, y);
            if entry != '.' {
                region[2][1] = entry;
                break;
            }

            if x == lines.len() - 1 {
                break;
            }

            x += 1;
        }

        if j < lines[i].len() - 1 {
            x = i + 1;
            y = j + 1;

            loop {
                let entry = get_entry(&lines, x, y);
                if entry != '.' {
                    region[2][2] = entry;
                    break;
                }

                if x == lines.len() -1 || y == lines[x].len() - 1 {
                    break;
                }

                x += 1;
                y += 1;
            }
        }
    }

    return region;
}

fn toggle2(region: &[[char; 3]; 3]) -> bool {
    match region[1][1] {
        'L' => {
            for i in 0..3 {
                for j in 0..3 {
                    if region[i][j] == '#' {
                        return false;
                    }
                }
            }
            return true;
        },
        '#' => {
            let mut count = 0;
            for i in 0..3 {
                for j in 0..3 {
                    if i == j && i == 1 {
                        continue;
                    }

                    if region[i][j] == '#' {
                        count += 1;
                        if count >= 5 {
                            return true;
                        }
                    }
                }
            }
            return false;
        },
        _ => return false,
    }
}

fn simulate2(values: &Vec::<String>) -> Vec::<String> {
    let mut state = values.to_vec();
    let mut next_state = Vec::<String>::new();

    loop {
        next_state.clear();
        let mut changed_state: bool = false;

        for i in 0..state.len() {
            let line = &state[i];
            let mut new_line = String::new();

            for j in 0..line.len() {
                let current: char = line.chars().nth(j).unwrap();
                match current {
                    'L' | '#' => {
                        if toggle2(&get_region2(&state, i , j)) {
                            if current == '#' {
                                new_line.push('L');
                            } else {
                                new_line.push('#');
                            }
                            changed_state = true;
                        } else {
                            new_line.push(current);
                        }
                    },
                    _ => new_line.push('.')
                }
            }

            next_state.push(new_line);
        }

        state = next_state.to_vec();

        // print_state(&state);

        if !changed_state {
            break;
        }
    }

    return state;
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values);
    assert_eq!(_result_first, 2338);
    let _result_second = solve_second(&values);
    assert_eq!(_result_second, 2134);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample() {
        let values = load_values(String::from("sample.txt"));
        let expected1 = 37;
        assert_eq!(solve_first(&values), expected1);
        let expected2 = 26;
        assert_eq!(solve_second(&values), expected2);
    }
}

