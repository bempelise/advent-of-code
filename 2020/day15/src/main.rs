use std::collections::HashMap;

struct Details {
    last: i32,
    before: Option<i32>,
}

fn solve_first(values: &Vec::<i32>) -> i32 {
    return simulate(values, 2020);
}

fn solve_second(values: &Vec::<i32>) -> i32 {
    return simulate(values, 30000000);
}

fn simulate(values: &Vec::<i32>, stop: i32) -> i32 {
    let mut memory = HashMap::<i32, Details>::new();
    let mut count = 1;
    let mut last_value = 0;

    for value in values {
        memory.insert(*value, Details {
            last: count,
            before: None
        });
        last_value = *value;
        count += 1;
    }

    loop {
        let new_value;

        match memory.get(&last_value).unwrap().before {
            None => new_value = 0,
            Some(before) => new_value = memory.get(&last_value).unwrap().last - before,
        }

        match memory.get_mut(&new_value) {
            Some(entry) => {
                entry.before = Some(entry.last);
                entry.last = count;
            },
            None => {
                memory.insert(new_value, Details {
                    last: count,
                    before: None,
                });
            },
        }

        last_value = new_value;

        if count == stop {
            break;
        }

        count += 1;

        // if count % 1000 == 0 {
        //     println!("Count: {}, Mem size: {}", count, memory.len());
        // }
    }

    return last_value;
}

fn main() {
    let values = vec![0,14,6,20,1,4];
    let result_first = solve_first(&values);
    println!("{}", result_first);
    assert_eq!(result_first, 257);
    let result_second = solve_second(&values);
    println!("{}", result_second);
    assert_eq!(result_second, 8546398);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_first() {
        let mut values = vec![0,3,6];
        assert_eq!(solve_first(&values), 436);
        values = vec![1,3,2];
        assert_eq!(solve_first(&values), 1);
        values = vec![2,1,3];
        assert_eq!(solve_first(&values), 10);
        values = vec![1,2,3];
        assert_eq!(solve_first(&values), 27);
        values = vec![2,3,1];
        assert_eq!(solve_first(&values), 78);
        values = vec![3,2,1];
        assert_eq!(solve_first(&values), 438);
        values = vec![3,1,2];
        assert_eq!(solve_first(&values), 1836);
    }

    #[test]
    fn test_solve_second1() {
        let values = vec![0,3,6];
        assert_eq!(solve_second(&values), 175594);
    }

    #[test]
    fn test_solve_second2() {
        let values = vec![1,3,2];
        assert_eq!(solve_second(&values), 2578);
    }

    #[test]
    fn test_solve_second3() {
        let values = vec![2,1,3];
        assert_eq!(solve_second(&values), 3544142);
    }

    #[test]
    fn test_solve_second4() {
        let values = vec![1,2,3];
        assert_eq!(solve_second(&values), 261214);
    }

    #[test]
    fn test_solve_second5() {
        let values = vec![2,3,1];
        assert_eq!(solve_second(&values), 6895259);
    }

    #[test]
    fn test_solve_second6() {
        let values = vec![3,2,1];
        assert_eq!(solve_second(&values), 18);
    }

    #[test]
    fn test_solve_second7() {
        let values = vec![3,1,2];
        assert_eq!(solve_second(&values), 362);
    }
}

