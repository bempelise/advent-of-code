use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::collections::HashMap;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<i32> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String").parse::<i32>().unwrap()).collect()
}

fn solve_first(values: &Vec::<i32>) -> i32 {
    let mut sorted = values.to_vec();
    sorted.sort();

    let mut one_jolt_diff = 0;
    let mut three_jolt_diff = 1;
    let mut current = 0;

    for value in sorted {
        match value - current {
            1 => one_jolt_diff += 1,
            3 => three_jolt_diff += 1,
            _ => ()
        }

        current = value;
    }

    let result = one_jolt_diff * three_jolt_diff;
    // println!("{}", result);
    return result;
}

fn solve_second(values: &Vec::<i32>) -> u64 {
    let mut sorted = values.to_vec();
    sorted.sort();

    let current = 0;

    let mut counts = HashMap::<String, u64>::new();

    let result = count_ways(&sorted[0..sorted.len()], current, &mut counts);

    println!("{}", result);
    return result;
}

fn count_ways(values: &[i32], current: i32, counts: &mut HashMap<String, u64>) -> u64 {
    // println!("{} - {:?}",current, values);
    if values.len() == 0 {
        return 1;
    }

    let mut count;

    let key = String::from(format!("{}-1", current));

    if counts.contains_key(&key) {
        count = *counts.get(&key).unwrap();
    } else {
        count = count_ways(&values[1..values.len()], values[0], counts);
        counts.insert(key, count);
        // println!("{} (1) {} Diff: {}", current,  count, values[0] - current);
    }


    if (values.len() > 1) && (values[1] - current <= 3) {
        let key2 = String::from(format!("{}-2", current));

        if counts.contains_key(&key2) {
            count += *counts.get(&key2).unwrap();
        } else {
            let count2 = count_ways(&values[2..values.len()], values[1], counts);
            counts.insert(key2, count2);
            // println!("{} (2) {} Diff: {}", current, count2, values[1] - current);
            count += count2;
        }

    }

    if (values.len() > 2) && (values[2] - current <= 3) {
        let key3 = String::from(format!("{}-3", current));
        if counts.contains_key(&key3) {
            count += *counts.get(&key3).unwrap();
        } else {
            let count3 = count_ways(&values[3..values.len()], values[2], counts);
            counts.insert(key3, count3);
            // println!("{} (3) {} Diff: {}", current, count3, values[2] - current);
            count += count3;
        }
    }

    return count;
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values);
    assert_eq!(_result_first, 1998);
    let _result_second = solve_second(&values);
    assert_eq!(_result_second, 347250213298688);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample() {
        let values_small = load_values(String::from("sample_small.txt"));
        let expected_small = 35;
        assert_eq!(solve_first(&values_small), expected_small);
        let values = load_values(String::from("sample.txt"));
        let expected1 = 220;
        assert_eq!(solve_first(&values), expected1);

        let expected_tiny = 6;
        let values_tiny = load_values(String::from("sample_tiny.txt"));
        assert_eq!(solve_second(&values_tiny), expected_tiny);
        let expected_small = 8;
        assert_eq!(solve_second(&values_small), expected_small);
        let expected2 = 19208;
        assert_eq!(solve_second(&values), expected2);
    }
}

