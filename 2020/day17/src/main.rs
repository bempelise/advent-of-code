use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::collections::HashSet;
// use std::cmp;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}


fn solve_first(values: &Vec::<String>) -> usize {
    let mut active = HashSet::<(i32, i32, i32)>::new();
    let mut x = 0;
    let mut y = 0;

    for line in values {
        for c in line.chars() {
            if c == '#' {
                active.insert((x, y, 0));
            }
            x += 1;
        }
        x = 0;
        y += 1;
    }

    return simulate3(&mut active);
}

fn simulate3(active: &mut HashSet::<(i32, i32, i32)>) -> usize {
    for i in 1..7 {
        println!("Cycle: {} [{}]", i, active.len());
        // println!("{:?}", active);
        let mut next = HashSet::<(i32, i32, i32)>::new();
        let mut min_z = -1;
        let mut max_z = 1;
        let mut min_y = i32::MAX;
        let mut max_y = i32::MIN;
        let mut min_x = i32::MAX;
        let mut max_x = i32::MIN;

        for cube in active.iter() {
            if cube.0 > max_x {
                max_x = cube.0;
            }

            if cube.0 < min_x {
                min_x = cube.0;
            }

            if cube.1 < min_y {
                min_y = cube.1;
            }

            if cube.1 > max_y {
                max_y = cube.1;
            }

            if cube.2 > max_z {
                max_z = cube.2;
            }

            if cube.2 < min_z {
                min_z = cube.2;
            }
        }

        println!("Z: {}-{}", min_z, max_z);
        println!("Y: {}-{}", min_y, max_y);
        println!("X: {}-{}", min_x, max_x);

        for z in min_z-1..max_z + 2 {
            for y in min_y-1..max_y + 2 {
                for x in min_x-1..max_x + 2 {
                    let mut active_neighs = 0;
                    for dz in -1..2 {
                        for dy in -1..2 {
                            for dx in -1..2 {
                                if dx != 0 || dy != 0 || dz != 0 {
                                    if active.contains(&(x + dx, y + dy, z + dz)) {
                                        active_neighs += 1;
                                    }
                                }
                            }
                        }
                    }

                    let is_active = active.contains(&(x, y, z));

                    if is_active {
                        if active_neighs == 2 || active_neighs == 3 {
                            next.insert((x, y, z));
                        }
                    } else {
                        if active_neighs == 3 {
                            next.insert((x, y, z));
                        }
                    }

                }
            }
        }

        active.clear();
        active.extend(next);

    }

    return active.len();
}

fn simulate4(active: &mut HashSet::<(i32, i32, i32, i32)>) -> usize {
    for i in 1..7 {
        println!("Cycle: {} [{}]", i, active.len());
        // println!("{:?}", active);
        let mut next = HashSet::<(i32, i32, i32, i32)>::new();
        let mut min_w = -1;
        let mut max_w = 1;
        let mut min_z = -1;
        let mut max_z = 1;
        let mut min_y = i32::MAX;
        let mut max_y = i32::MIN;
        let mut min_x = i32::MAX;
        let mut max_x = i32::MIN;

        for cube in active.iter() {
            if cube.0 > max_x {
                max_x = cube.0;
            }

            if cube.0 < min_x {
                min_x = cube.0;
            }

            if cube.1 < min_y {
                min_y = cube.1;
            }

            if cube.1 > max_y {
                max_y = cube.1;
            }

            if cube.2 > max_z {
                max_z = cube.2;
            }

            if cube.2 < min_z {
                min_z = cube.2;
            }

            if cube.3 > max_w {
                max_w = cube.3;
            }

            if cube.3 < min_w {
                min_w = cube.3;
            }
        }

        println!("Z: {}-{}", min_w, max_w);
        println!("Z: {}-{}", min_z, max_z);
        println!("Y: {}-{}", min_y, max_y);
        println!("X: {}-{}", min_x, max_x);

        for w in min_w-1..max_w + 2 {
            for z in min_z-1..max_z + 2 {
                for y in min_y-1..max_y + 2 {
                    for x in min_x-1..max_x + 2 {
                        let mut active_neighs = 0;
                        for dw in -1..2 {
                            for dz in -1..2 {
                                for dy in -1..2 {
                                    for dx in -1..2 {
                                        if dx != 0 || dy != 0 || dz != 0 || dw != 0 {
                                            if active.contains(&(x + dx, y + dy, z + dz, w + dw)) {
                                                active_neighs += 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        let is_active = active.contains(&(x, y, z, w));

                        if is_active {
                            if active_neighs == 2 || active_neighs == 3 {
                                next.insert((x, y, z, w));
                            }
                        } else {
                            if active_neighs == 3 {
                                next.insert((x, y, z, w));
                            }
                        }

                    }
                }
            }
        }

        active.clear();
        active.extend(next);

    }

    return active.len();
}

fn solve_second(values: &Vec::<String>) -> usize {
    let mut active = HashSet::<(i32, i32, i32, i32)>::new();
    let mut x = 0;
    let mut y = 0;

    for line in values {
        for c in line.chars() {
            if c == '#' {
                active.insert((x, y, 0, 0));
            }
            x += 1;
        }
        x = 0;
        y += 1;
    }

    return simulate4(&mut active);
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let result_first = solve_first(&values);
    println!("{}", result_first);
    assert_eq!(result_first, 313);
    let result_second = solve_second(&values);
    println!("{}", result_second);
    // assert_eq!(result_second, 0);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_first() {
        let values = load_values(String::from("sample.txt"));
        assert_eq!(solve_first(&values), 112);
    }

    #[test]
    fn test_solve_second() {
        let values = load_values(String::from("sample.txt"));
        assert_eq!(solve_second(&values), 848);
    }
}

