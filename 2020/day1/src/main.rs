use std::io::{Error, BufRead, BufReader};
use std::fs::File;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<i32> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String").parse::<i32>().unwrap()).collect()
}

fn solve_first(values: &Vec::<i32>, expected: i32) {
    let target = 2020;
    let pair = find_pair(&values, target, None);

    if pair != (0, 0) {
        let result = values[pair.0] * values[pair.1];
        println!("{}", result);
        assert_eq!(expected, result);
    }
}

fn solve_second(values: &Vec::<i32>, expected: i32) {
    let target = 2020;
    for i in 0..values.len() {
        let pair = find_pair(&values, target - values[i], Some(i));

        if pair != (0, 0) {
            let result = values[i] * values[pair.0] * values[pair.1];
            println!("{}", result);
            assert_eq!(expected, result);
            break;
        }
    }
}

fn find_pair(values: &Vec::<i32>, target: i32, exception: Option::<usize>) -> (usize, usize) {

    for i in 0..values.len() {
        if skip(i, exception) {
            continue;
        }

        for j in i..values.len() {
            if skip(j, exception) {
                continue;
            }

            if values[i] + values[j] == target {
                return (i.try_into().unwrap(), j.try_into().unwrap());
            }
        }
    }

    return (0, 0)
}

fn skip (index: usize, exception: Option::<usize>) -> bool {
    return exception.unwrap_or(usize::MAX) == index;
}

fn main() {
    let do_sample: bool = false;
    if do_sample {
        let values = load_values(String::from("sample.txt"));
        let expected_first = 514579;
        let expected_second = 241861950;
        solve_first(&values, expected_first);
        solve_second(&values, expected_second);
    } else {
        let values = load_values(String::from("input.txt"));
        let expected_first = 55776;
        let expected_second = 223162626;
        solve_first(&values, expected_first);
        solve_second(&values, expected_second);
    }
}
