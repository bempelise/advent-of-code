use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::collections::HashSet;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}


#[derive(Copy, Clone)]
enum OpCode{
    Nop,
    Acc,
    Jmp
}

#[derive(Copy, Clone)]
struct Command {
    opcode: OpCode,
    value: i32,
    position: i32,
}

fn solve_first(values: &Vec::<String>) -> i32 {
    let commands = get_commands(&values);
    let (accumulator, _) = execute(&commands);
    // println!("{}", accumulator);
    return accumulator;
}

fn solve_second(values: &Vec::<String>) -> i32 {
    let commands = get_commands(&values);
    let hist = execute_with_history(&commands);

    for command in hist {
        let mut altered = commands.clone();
        let toggled = match command.opcode {
            OpCode::Nop => Command {
                opcode: OpCode::Jmp,
                position: command.position,
                value: command.value
            },
            OpCode::Jmp => Command {
                opcode: OpCode::Nop,
                position: command.position,
                value: command.value
            },
            OpCode::Acc => panic!("Acc found in history")
        };

        altered[command.position as usize] = toggled;
        let (accumulator, completed) = execute(&altered);
        if completed {
            println!("{}", accumulator);
            return accumulator;
        }
    }

    panic!("Failed to solve second");
}

fn get_commands(lines: &Vec::<String>) -> Vec::<Command> {
    let mut commands = Vec::<Command>::new();

    let mut position: i32 = 0;
    for line in lines {
        commands.push(parse_command(&line, &position));
        position += 1;
    }

    return commands;
}

fn parse_command(line: &String, position: &i32) -> Command {
    let parts: Vec::<&str> = line.split(' ').collect();
    Command {
        opcode: match parts[0] {
            "nop" => OpCode::Nop,
            "acc" => OpCode::Acc,
            "jmp" => OpCode::Jmp,
            _ => panic!("Unknown opcode {:?}", parts[0])
        },
        value: parts[1].parse::<i32>().unwrap(),
        position: *position
    }
}

fn execute(commands: &Vec::<Command>) -> (i32, bool) {
    let mut prog_counter : i32 = 0;
    let mut accumulator: i32 = 0;
    let mut seen = HashSet::<i32>::new();
    let mut completed = false;

    loop {
        if seen.contains(&prog_counter)
        {
            println!("Breaking, infinite loop detected at {}", prog_counter);
            break;
        }

        if prog_counter as usize >= commands.len() {
            println!("Breaking, end of program");
            completed = true;
            break;
        }

        seen.insert(prog_counter);

        let command : &Command = &commands[prog_counter as usize];

        match command.opcode {
            OpCode::Nop => {
                prog_counter += 1;
            },
            OpCode::Acc => {
                prog_counter += 1;
                accumulator += command.value;
            },
            OpCode::Jmp => {
                prog_counter += command.value;
            }
        }
    }

    return (accumulator, completed);
}

fn execute_with_history(commands: &Vec::<Command>) -> Vec::<Command> {
    let mut prog_counter : i32 = 0;
    let mut seen = HashSet::<i32>::new();
    let mut hist = Vec::<Command>::new();

    loop {
        if seen.contains(&prog_counter)
        {
            println!("Breaking, infinite loop detected at {}", prog_counter);
            break;
        }

        if prog_counter as usize >= commands.len() {
            println!("Breaking, end of program");
            break;
        }

        seen.insert(prog_counter);

        let command : &Command = &commands[prog_counter as usize];

        match command.opcode {
            OpCode::Nop => {
                prog_counter += 1;
                hist.push(*command);
            },
            OpCode::Acc => {
                prog_counter += 1;
            },
            OpCode::Jmp => {
                prog_counter += command.value;
                hist.push(*command);
            }
        }
    }

    return hist;
}


fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values);
    assert_eq!(_result_first, 1810);
    let _result_second = solve_second(&values);
    assert_eq!(_result_second, 969);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample() {
        let values = load_values(String::from("sample.txt"));
        let expected1 = 5;
        assert_eq!(solve_first(&values), expected1);
        let expected2 = 8;
        assert_eq!(solve_second(&values), expected2);
    }
}

