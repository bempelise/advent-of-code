use std::io::{Error, BufRead, BufReader};
use std::fs::File;

struct Entry {
    letter: char,
    min: i32,
    max: i32,
    password: String,
}

fn load_entry(line: String) -> Entry
{
    let parts: Vec<&str> = line.split(['-', ' ', ':']).collect();

    Entry {
        letter: parts[2].chars().next().expect("No letter given"),
        min: parts[0].parse::<i32>().unwrap(),
        max: parts[1].parse::<i32>().unwrap(),
        password: parts[4].to_string(),
    }
}

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<Entry> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| load_entry(l.ok().expect("Line not a String"))).collect()
}

fn solve_first(values: &Vec::<Entry>, expected: i32) {
    let mut valid: i32 = 0;

    for entry in values {
        let count = count_chars(&entry.password, entry.letter);

        if count >= entry.min && count <= entry.max {
            valid += 1;
        }
    }

    assert_eq!(expected, valid);
}

fn solve_second(values: &Vec::<Entry>, expected: i32) {
    let mut valid: i32 = 0;

    for entry in values {
        let is_in_pos1 : bool = entry.password.chars().nth(entry.min as usize - 1).expect("String does not contain min") == entry.letter;
        let is_in_pos2 : bool = entry.password.chars().nth(entry.max as usize - 1).expect("String does not contain max") == entry.letter;

        if is_in_pos1 ^ is_in_pos2 {
            valid += 1;
        }
    }

    assert_eq!(expected, valid);
}

fn count_chars(password: &String, target: char) -> i32 {
    let mut counter: i32 = 0;

    for c in password.chars() {
        if c == target {
            counter += 1;
        }
    }

    return counter;
}

fn main() {
    let do_sample: bool = false;
    if do_sample {
        let values = load_values(String::from("sample.txt"));
        let expected_first = 2;
        let expected_second = 1;
        solve_first(&values, expected_first);
        solve_second(&values, expected_second);
    } else {
        let values = load_values(String::from("input.txt"));
        let expected_first = 477;
        let expected_second = 686;
        solve_first(&values, expected_first);
        solve_second(&values, expected_second);
    }
}
