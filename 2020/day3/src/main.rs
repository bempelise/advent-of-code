use std::io::{Error, BufRead, BufReader};
use std::fs::File;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>, expected: i32) {
    let tree_count = slope(&values, 3, 1);

    assert_eq!(expected, tree_count);
}

fn solve_second(values: &Vec::<String>, expected: u64) {
    let mut product: u64 = 1;
    product *= slope(&values, 1, 1) as u64;
    product *= slope(&values, 3, 1) as u64;
    product *= slope(&values, 5, 1) as u64;
    product *= slope(&values, 7, 1) as u64;
    product *= slope(&values, 1, 2) as u64;

    assert_eq!(expected, product);
}

fn slope(values: &Vec::<String>,  step_x: usize, step_y: usize) -> i32 {
    let width = values[0].chars().count();
    let height = values.len();

    let mut tree_count = 0;
    let mut x = step_x;
    let mut y = step_y;

    while y < height {
        if is_tree(&values, x % width, y) {
            tree_count += 1;
        }

        x += step_x;
        y += step_y;
    }

    return tree_count;
}

fn is_tree(values: &Vec::<String>, x: usize, y: usize) -> bool {
    // println!("x: {}, y: {}", x, y);
    return *(&values[y].chars().nth(x).unwrap()) == '#';
}

fn main() {
    let do_sample: bool = false;
    if do_sample {
        let values = load_values(String::from("sample.txt"));
        let expected_first = 7;
        let expected_second = 336;
        solve_first(&values, expected_first);
        solve_second(&values, expected_second);
    } else {
        let values = load_values(String::from("input.txt"));
        let expected_first = 200;
        let expected_second = 3737923200;
        solve_first(&values, expected_first);
        solve_second(&values, expected_second);
    }
}
