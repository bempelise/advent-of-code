use std::io::{Error, BufRead, BufReader};
use std::fs::File;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>) -> i32 {
    let mut result = 0;

    for value in values {
        let id = get_id(value);
        if id > result {
            result = id;
        }
    }

    // println!("{}", result);
    return result;
}

fn solve_second(values: &Vec::<String>) -> i32 {
    let mut result = 0;

    let mut seats = Vec::<i32>::new();

    for value in values {
        seats.push(get_id(value));
    }

    seats.sort();

    let mut previous = None;
    for seat in seats {
        match previous {
            Some(prev_seat) => {
                if seat - 2 == prev_seat {
                    result = seat - 1;
                    break;
                }
            },
            None => ()
        }

        previous = Some(seat);
        // println!("{}", seat);
    }

    println!("{}", result);
    return result;
}

fn get_id(seat: &String) -> i32 {
    let row = get_row(seat);
    let col = get_col(seat);
    // println!("({}, {})", row, col);
    return row * 8 + col;
}

fn get_row(seat: &String) -> i32 {
    let mut lower = 0;
    let mut upper = 127;
    for c in seat[..6].chars() {
        match c {
            'F' => upper = lower + (upper - lower)/2,
            'B' => lower = lower + (upper - lower + 1)/2,
            _ => panic!("Invalid character: {}", c)
        }

        // println!("({}, {})", lower, upper);
    }

    if seat.chars().nth(6) == Some('F') {
        return lower;
    }
    return upper;
}

fn get_col(seat: &String) -> i32 {
    let mut lower = 0;
    let mut upper = 7;
    for c in seat[7..9].chars() {
        match c {
            'L' => upper = lower + (upper - lower)/2,
            'R' => lower = lower + (upper - lower + 1)/2,
            _ => panic!("Invalid character: {}", c)
        }

        // println!("({}, {})", lower, upper);
    }

    if seat.chars().nth(9) == Some('L') {
        return lower;
    }
    return upper;
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let result_first = solve_first(&values);
    assert_eq!(result_first, 826);
    let _result_second = solve_second(&values);
    assert_eq!(result_second, 678);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_id() {
        let values = load_values(String::from("sample.txt"));
        let expected = [357, 567, 119, 820];

        for i in 0..values.len() {
            assert_eq!(get_id(&values[i]), expected[i]);
        }
    }
}
