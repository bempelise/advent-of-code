use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::collections::HashMap;
use std::collections::HashSet;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>) -> i32 {
    let rules = parse_rules(&values);

    let eligible = containers_for("shiny gold", &rules);

    let result = eligible.len() as i32;

    println!("{}", result);
    return result;
}

fn solve_second(values: &Vec::<String>) -> i32 {
    let rules = parse_rules(&values);

    let result = bags_needed_for("shiny gold", &rules);
    println!("{}", result);
    return result;
}

fn parse_rules(values: &Vec::<String>) -> HashMap::<String, HashMap::<String, i32>> {
    let mut rules = HashMap::<String, HashMap::<String, i32>>::new();

    for line in values {
        let parsed = parse_rule(line);
        rules.insert(parsed.0, parsed.1);
    }

    return rules;
}

fn bags_needed_for(bag: &str, rules: &HashMap::<String, HashMap::<String, i32>>) -> i32 {
    let mut total = 0;

    for (entry, count) in &rules[bag] {
        total += count;
        total += bags_needed_for(&entry, &rules) * count;
    }

    return total;
}

fn containers_for<'a>(bag: &'a str, rules: &'a HashMap::<String, HashMap::<String, i32>>) -> HashSet::<&'a str> {
    let mut direct = HashSet::<&str>::new();
    let mut seen = HashSet::<&str>::new();

    for (container, contents) in rules {
        if contents.contains_key(bag) {
            // println!("{}", container);
            direct.insert(container);
            seen.insert(container);
        }
    }

    for container in direct {
        seen = seen.union(&containers_for(container, &rules)).copied().collect();
    }

    return seen;
}

fn parse_rule(line: &String) -> (String, HashMap<String, i32>) {
    let mut rule = line.replace(" bags", "");
    rule = rule.replace(" bag", "");
    rule = rule.replace(".", "");

    let parsed_rule : Vec::<&str> = rule.split(" contain ").collect();
    let container = parsed_rule[0];
    let parsed_contents : Vec::<&str> = parsed_rule[1].split(", ").collect();

    let mut contents = HashMap::<String, i32>::new();

    for entry in parsed_contents {
        let first_space = entry.find(' ').unwrap_or(entry.len());
        let mut entry_name = entry.to_string();
        let quantity_str: String = entry_name.drain(..first_space).collect();
        entry_name = entry_name.trim().to_string();

        match quantity_str.parse::<i32>() {
            Ok(num) => contents.insert(entry_name, num),
            Err(_) => None
        };
    }

    return (container.to_string(), contents);
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values);
    assert_eq!(_result_first, 205);
    let _result_second = solve_second(&values);
    assert_eq!(_result_second, 80902);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample() {
        let values = load_values(String::from("sample.txt"));
        let expected1 = 4;
        assert_eq!(solve_first(&values), expected1);
        let expected2 = 32;
        assert_eq!(solve_second(&values), expected2);
    }
}

