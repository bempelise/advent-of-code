use std::io::{Error, BufRead, BufReader};
use std::fs::File;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<u64> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String").parse::<u64>().unwrap()).collect()
}

fn has_pair(values: &[u64], target: u64) -> bool {

    for i in 0..values.len() {
        for j in i..values.len() {
            if values[i] + values[j] == target {
                return true;
            }
        }
    }

    return false;
}

fn solve_first(values: &Vec::<u64>, window: usize) -> u64 {

    for i in window..values.len() {
        if !has_pair(&values[i-window..i], values[i]) {
            println!("{}", &values[i]);
            return values[i];
        }
    }
    println!("Not found");
    return 0;
}

fn sum(values :&[u64]) -> u64 {
    let mut sum = 0;

    for value in values {
        sum += value
    }

    return sum;
}

fn min(values :&[u64]) -> u64 {
    let mut min = std::u64::MAX;

    for value in values {
        if min > *value {
            min = *value;
        }
    }

    return min;
}

fn max(values :&[u64]) -> u64 {
    let mut max = 0;

    for value in values {
        if max < *value {
            max = *value;
        }
    }

    return max;
}

fn solve_second(values: &Vec::<u64>, target: u64) -> u64 {
    let mut start = 0;
    let mut end = 2;

    loop {
        // println!("({}, {})", start, end);

        let sum = sum(&values[start..end]);
        // println!("{:?}", sum);

        if sum == target {
            let min = min(&values[start..end]);
            let max = max(&values[start..end]);
            println!("{}", min + max);
            return min + max;
        }
        else if sum > target {
            start += 1;
        }
        else {
            end += 1;
        }

        if target == values[(end + 1) as usize] {
            println!("Failed");
            return 0;
        }
    }
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values, 25);
    assert_eq!(_result_first, 776203571);
    let _result_second = solve_second(&values, _result_first);
    assert_eq!(_result_second, 104800569);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample() {
        let values = load_values(String::from("sample.txt"));
        let expected1 = 127;
        assert_eq!(solve_first(&values, 5), expected1);
        let expected2 = 62;
        assert_eq!(solve_second(&values, expected1), expected2);
    }
}

