use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::collections::HashMap;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>) -> u64 {
    let mut mask_and : u64 = 0xFFFFFFFFFFFFFFFF;
    let mut mask_or : u64 = 0;
    let mut memory = HashMap::<u64, u64>::new();

    for line in values {
        let contents : Vec<&str> = line.split('=').collect();

        if contents[0].trim() == "mask" {
            (mask_and, mask_or) = get_masks_v1(contents[1]);
        }
        else {
            let address = get_address(contents[0]);
            let mut value = contents[1].trim().parse::<u64>().unwrap();
            value = value & mask_and;
            value = value | mask_or;

            memory.insert(address, value);
        }
    }

    let mut sum = 0;
    for (_, value) in &memory {
        sum += value;
    }

    println!("{}", sum);
    return sum;
}

fn get_masks_v1(mask: &str) -> (u64, u64) {
    let mut mask_and : u64 = 0xFFFFFFFFFFFFFFFF;
    let mut mask_or : u64 = 0;

    let mut position = mask.trim().len();

    for c in mask.trim().chars() {
        position -= 1;

        match c {
            '0' => {
                mask_and = mask_and & !(1 << position);
            },
            '1' => {
                mask_or = mask_or | 1 << position;
            },
            _ => ()
        }
    }

    return (mask_and, mask_or);
}

fn get_address(address: &str) -> u64 {
    let contents: Vec<&str> = address.split(&['[', ']']).collect();
    return contents[1].parse::<u64>().unwrap();
}

fn solve_second(values: &Vec::<String>) -> u64 {
    let mut mask = String::new();
    let mut memory = HashMap::<u64, u64>::new();

    for line in values {
        let contents : Vec<&str> = line.split('=').collect();

        if contents[0].trim() == "mask" {
            mask = contents[1].trim().to_string()
        }
        else {
            let address = get_address(contents[0]);
            let addresses: Vec<u64> = get_addresses(address, &mask);
            let value = contents[1].trim().parse::<u64>().unwrap();

            for addr in addresses {
                memory.insert(addr, value);
            }
        }
    }

    let mut sum = 0;
    for (_, value) in &memory {
        sum += value;
    }

    println!("{}", sum);
    return sum;
}

fn get_addresses(address: u64, mask: &String) -> Vec<u64> {
    let mut addresses = Vec::<u64>::new();

    addresses.push(address);

    let mut position = 0;
    let mut bit_position = mask.len();
    let mut new_mask = mask.clone();

    // println!("{:?}", new_mask);
    // println!("{:?}", addresses);

    for c in mask.chars() {
        bit_position -= 1;
        match c {
            '1' => {
                for i in 0..addresses.len()
                {
                    addresses[i] |= 1 << bit_position;
                }

                new_mask.replace_range(position..(position + 1), "0");
                // println!("After '1' {:?}", new_mask);
                // println!("{:?}", addresses);
            },
            'X' => {
                let mut new_addresses = Vec::<u64>::new();

                for addr in &addresses {
                    new_addresses.push(addr | 1 << bit_position);
                    new_addresses.push(addr & !(1 << bit_position));
                }

                addresses.clear();
                addresses.append(&mut new_addresses);

                new_mask.replace_range(position..(position + 1), "0");
                // println!("After 'X' {:?}", new_mask);
                // println!("{:?}", addresses);
            }
            _ => ()
        }
        position += 1;
    }


    return addresses;
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values);
    assert_eq!(_result_first, 5875750429995);
    let _result_second = solve_second(&values);
    assert_eq!(_result_second, 5272149590143);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_first() {
        let values = load_values(String::from("sample.txt"));
        let expected = 165;
        assert_eq!(solve_first(&values), expected);
    }
    #[test]
    fn test_solve_second() {
        let values = load_values(String::from("sample2.txt"));
        let expected = 208;
        assert_eq!(solve_second(&values), expected);
    }
}

