use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::collections::HashSet;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

struct Field {
    name: String,
    reqs: Vec<Req>
}

impl Field {
    fn sat(&self, input: i32) -> bool {
        let mut ok = false;
        for req in &self.reqs {
            ok = ok || req.sat(input);
        }
        return ok;
    }
}

struct Req {
    min: i32,
    max: i32,
}

impl Req {
    fn sat(&self, input: i32) -> bool {
        return input >= self.min && input <= self.max;
    }
}

enum ParseState {
    Fields,
    MyTicket,
    NearBy,
}

#[derive(Debug)]
struct Ticket {
    entries: Vec<i32>,
}

fn solve_first(values: &Vec::<String>) -> i32 {
    let mut fields = Vec::<Field>::new();
    let mut sum = 0;

    let mut state = ParseState::Fields;

    for line in values {
        match state {
            ParseState::Fields => {
                if line == "" {
                    state = ParseState::MyTicket;
                    continue;
                }

                fields.push(parse_field(line));
            },
            ParseState::MyTicket => {
                if line == "" {
                    state = ParseState::NearBy;
                    continue;
                }
                continue;
            },
            ParseState::NearBy => {
                if line == "nearby tickets:" {
                    continue;
                }

                let mut ticket = Ticket::new();
                ticket.add_entry(line);
                sum += ticket.invalid_values(&fields);
            }
        }
    }

    return sum;
}

fn parse_field(line: &str) -> Field
{
    let contents : Vec<&str> = line.split(':').collect();
    let name = contents[0].trim();

    let rules: Vec<&str> = contents[1].split(" or ").collect();

    let mut reqs = Vec::<Req>::new();

    for rule in &rules {
        let boundaries: Vec<&str> = rule.split('-').collect();

        reqs.push(Req{
            min: boundaries[0].trim().parse::<i32>().unwrap(),
            max: boundaries[1].trim().parse::<i32>().unwrap(),
        });
    }


    Field {
        name: name.to_string(),
        reqs: reqs,
    }
}

impl Ticket {
    fn add_entry(&mut self, line: &str) {
        let values: Vec<&str> = line.split(',').collect();

        for value in values {
            self.entries.push(value.trim().parse::<i32>().unwrap());
        }
    }

    fn new() -> Ticket {
        Ticket {
            entries: Vec::<i32>::new()
        }
    }

    fn invalid_values(&self, fields: &Vec<Field>) -> i32
    {
        let mut sum = 0;

        for number in &self.entries {

            let mut ok = false;
            for field in fields {
                ok = ok || field.sat(*number);
            }

            if !ok {
                sum += *number;
            }
        }

        return sum;
    }

    fn has_invalid(&self, fields: &Vec<Field>) -> bool
    {
        for number in &self.entries {

            let mut ok = false;
            for field in fields {
                ok = ok || field.sat(*number);
            }

            if !ok {
                return true;
            }
        }

        return false;
    }
}

fn solve_second(values: &Vec::<String>) -> u64 {
    let mut fields = Vec::<Field>::new();
    let mut tickets = Vec::<Ticket>::new();
    let mut state = ParseState::Fields;
    let mut myticket = Ticket::new();

    for line in values {
        match state {
            ParseState::Fields => {
                if line == "" {
                    state = ParseState::MyTicket;
                    continue;
                }

                fields.push(parse_field(line));
            },
            ParseState::MyTicket => {
                if line == "" {
                    state = ParseState::NearBy;
                    continue;
                }

                if line == "your ticket:" {
                    continue;
                }

                myticket.add_entry(line);

                continue;
            },
            ParseState::NearBy => {
                if line == "nearby tickets:" {
                    continue;
                }

                let mut ticket = Ticket::new();
                ticket.add_entry(line);

                if ticket.has_invalid(&fields) {
                    continue;
                }
                else {
                    tickets.push(ticket);
                }
            }
        }
    }


    let mut possibles = init_field_names(&fields, myticket.entries.len());

    remove_invalid_names(&fields, &tickets, &mut possibles);

    remove_unique_entries(&mut possibles);

    for i in 0..possibles.len() {
        println!("{}: {:?}", i, possibles[i]);
    }

    let mut product: u64 = 1;
    for i in 0..possibles.len() {
        assert_eq!(possibles[i].len(), 1);

        let field_name = possibles[i].iter().next().unwrap().clone();

        if field_name.starts_with("departure") {
            product *= myticket.entries[i] as u64;
        }
    }

    return product;
}

fn init_field_names(fields: &Vec::<Field>, length: usize) -> Vec::<HashSet<String>> {
    let mut possibles = Vec::<HashSet<String>>::new();
    let mut all_values = HashSet::<String>::new();

    for field in fields {
        all_values.insert(field.name.to_string());
    }

    for _ in 0..length {
        possibles.push(all_values.clone());
    }

    // println!{"{:?}", possibles};

    return possibles;
}

fn remove_invalid_names(fields: &Vec::<Field>, tickets: &Vec::<Ticket>, possibles: &mut Vec::<HashSet<String>>) {
    for ticket in tickets {
        for i in 0..ticket.entries.len() {
            for field in fields {
                if !field.sat(ticket.entries[i]) {
                    possibles[i].remove(&field.name);
                    // println!("Removing {} from {} because it is not satisfied", field.name, i);

                    if possibles[i].len() == 1 {
                        let remaining = possibles[i].iter().next().unwrap().clone();
                        for j in 0..possibles.len() {
                            if j == i {
                                continue;
                            }
                            possibles[j].remove(&remaining);
                            // println!("Removing {} from {} because it is unique in {}", remaining, j, i);
                        }
                    }
                }
            }
        }
    }
}

fn remove_unique_entries(possibles: &mut Vec::<HashSet<String>>) {

    loop {
        let mut has_unique = false;

        for i in 0..possibles.len() {
            if possibles[i].len() == 1 {
                continue;
            }

            let mut unique_values = Vec::<String>::new();
            for value in possibles[i].iter() {
                let mut is_unique = true;
                for j in 0..possibles.len() {
                    if j == i {
                        continue;
                    }

                    if possibles[j].contains(value) {
                        is_unique = false;
                        break;
                    }
                }

                if is_unique {
                    unique_values.push(value.to_string());
                }
            }

            if unique_values.len() > 0 {
                possibles[i].clear();
                for value in unique_values {
                    possibles[i].insert(value);
                }
                has_unique = true;
            }
        }


        if !has_unique {
            break;
        }
    }
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let result_first = solve_first(&values);
    println!("{}", result_first);
    assert_eq!(result_first, 19087);
    let result_second = solve_second(&values);
    println!("{}", result_second);
    assert_eq!(result_second, 1382443095281);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_first() {
        let values = load_values(String::from("sample.txt"));
        assert_eq!(solve_first(&values), 71);
    }

    #[test]
    fn test_solve_second() {
        let values = load_values(String::from("sample2.txt"));
        assert_eq!(solve_second(&values), 156);
    }
}

