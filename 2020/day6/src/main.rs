use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::collections::HashSet;
use std::collections::HashMap;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>) -> i32 {
    let mut result = 0;

    let mut answers = HashSet::new();

    for line in values {
        if line == "" {
            result += answers.len();
            answers.clear();
        } else {
            for c in line.chars() {
                answers.insert(c);
            }
        }
    }

    result += answers.len();

    // println!("{}", result);
    return result as i32;
}

fn solve_second(values: &Vec::<String>) -> i32 {
    let mut result = 0;

    let mut answers = HashMap::<char, i32>::new();
    let mut group_size : i32 = 0;

    for line in values {
        if line == "" {
            // println!("{:?}", &answers);
            // println!("{:?}", group_size);

            for (_, value) in &answers {
                if *value == group_size {
                    result += 1;
                }
            }
            answers.clear();
            group_size = 0;
        } else {
            group_size += 1;
            for c in line.chars() {
                *answers.entry(c).or_insert(0) += 1;
            }
        }
    }

    for (_, value) in &answers {
        if *value == group_size {
            result += 1;
        }
    }

    println!("{}", result);
    return result;
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values);
    assert_eq!(_result_first, 6430);
    let _result_second = solve_second(&values);
    assert_eq!(_result_second, 3125);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_id() {
        let values = load_values(String::from("sample.txt"));
        let expected1 = 11;
        assert_eq!(solve_first(&values), expected1);
        let expected2 = 6;
        assert_eq!(solve_second(&values), expected2);
    }
}

