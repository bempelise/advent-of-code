use std::io::{Error, BufRead, BufReader};
use std::fs::File;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>) -> i32 {
    let start = values[0].parse::<i32>().unwrap();
    let buses = values[1].split(',');

    let mut best_bus = 0;
    let mut best_minutes = std::i32::MAX;

    for bus in buses {
        if bus == "x" {
            continue;
        }

        let bus_num = bus.parse::<i32>().unwrap();
        let minutes = bus_num - start % bus_num;

        println!("Bus: {}, Minutes: {}", bus_num, minutes);

        if minutes < best_minutes {
            best_minutes = minutes;
            best_bus = bus_num;
        }
    }

    let result = best_bus * best_minutes;
    println!("{}", result);
    return result;
}

 #[derive(Debug)]
struct Requirement {
    b: u64,
    n: u64,
}

fn solve_second(values: &Vec::<String>) -> u64 {
    let buses = values[1].split(',');

    let mut minute = 0;
    let mut requirements = Vec::<Requirement>::new();


    for bus in buses {
        if bus == "x" {
            minute += 1;
            continue;
        }

        let period = bus.parse::<u64>().unwrap();

        requirements.push(Requirement {
            b: (period - minute % period),
            n: period as u64,
        });

        println!("{:?}", requirements[requirements.len()-1]);

        minute += 1;
    }


    let result = chinese_remainder_theorem(&requirements);
    println!("{}", result);
    return result;
}

fn chinese_remainder_theorem(requirements: &Vec::<Requirement>) -> u64 {

    let mut n_t: u64 = 1;
    for req in requirements {
        n_t *= req.n;
    }

    let mut sum: u64 = 0;

    for req in requirements {
        let n_i = n_t/req.n;
        let x_i = find_x(n_i, req.n);
        let prod: u64 = req.b * n_i * x_i;
        sum += prod as u64;
    }

    return sum % n_t;
}

fn find_x(n_i: u64, n: u64) -> u64 {
    let n_mod = n_i % n;
    let mut result = 1;

    loop {
        if (n_mod * result) % n == 1 {
            return result;
        }

        result += 1;
    }
}


fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values);
    assert_eq!(_result_first, 2238);
    let _result_second = solve_second(&values);
    assert_eq!(_result_second, 560214575859998);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample() {
        let values = load_values(String::from("sample.txt"));
        let expected1 = 295;
        assert_eq!(solve_first(&values), expected1);
        let expected2 = 1068781;
        // let expected2 = 0;
        assert_eq!(solve_second(&values), expected2);

        let values1 = load_values(String::from("sample1.txt"));
        assert_eq!(solve_second(&values1), 3417);
        let values2 = load_values(String::from("sample2.txt"));
        assert_eq!(solve_second(&values2), 754018);
        let values3 = load_values(String::from("sample3.txt"));
        assert_eq!(solve_second(&values3), 779210);
        let values4 = load_values(String::from("sample4.txt"));
        assert_eq!(solve_second(&values4), 1261476);
        let values5 = load_values(String::from("sample5.txt"));
        assert_eq!(solve_second(&values5), 1202161486);
    }

    #[test]
    fn test_find_x() {
        assert_eq!(find_x(56 ,5), 1);
        assert_eq!(find_x(40 ,7), 3);
        assert_eq!(find_x(35 ,8), 3);
    }
}

