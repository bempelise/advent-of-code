use std::io::{Error, BufRead, BufReader};
use std::fs::File;
use std::f32::consts::PI;

fn read_input(filepath: String) -> Result<BufReader<File>, Error>
{
    let file_result = File::open(filepath);

    match file_result {
        Ok(file) => return Ok(BufReader::new(file)),
        Err(error) => return Err(error)
    };
}

fn load_values(filepath: String) -> Vec::<String> {
    let read_input_result = read_input(filepath);

    let reader = match read_input_result {
        Ok(reader) => reader,
        Err(error) => panic!("Failed to open input: {:?}", error)
    };

    reader.lines().map(|l| l.ok().expect("Line not a String")).collect()
}

fn solve_first(values: &Vec::<String>) -> i32 {
    let mut x = 0;
    let mut y = 0;
    let mut direction: char = 'E';

    for line in values {
        let instruction = parse_instruction(&line);
        match instruction.directive {
            'N' => y += instruction.value,
            'S' => y -= instruction.value,
            'E' => x += instruction.value,
            'W' => x -= instruction.value,
            'L' => {
                match instruction.value % 360 {
                    0 => (),
                    90 => match direction {
                        'N' => direction = 'W',
                        'S' => direction = 'E',
                        'E' => direction = 'N',
                        'W' => direction = 'S',
                        _ => panic!("Not happening"),
                    },
                    180 => match direction {
                        'N' => direction = 'S',
                        'S' => direction = 'N',
                        'E' => direction = 'W',
                        'W' => direction = 'E',
                        _ => panic!("Not happening"),
                    },
                    270 => match direction {
                        'N' => direction = 'E',
                        'S' => direction = 'W',
                        'E' => direction = 'S',
                        'W' => direction = 'N',
                        _ => panic!("Not happening"),
                    },
                    _ => panic!("Unsupported direction {}{}", instruction.directive, instruction.value)
                }
            },
            'R' => {
                match instruction.value % 360 {
                    0 => (),
                    270 => match direction {
                        'N' => direction = 'W',
                        'S' => direction = 'E',
                        'E' => direction = 'N',
                        'W' => direction = 'S',
                        _ => panic!("Not happening"),
                    },
                    180 => match direction {
                        'N' => direction = 'S',
                        'S' => direction = 'N',
                        'E' => direction = 'W',
                        'W' => direction = 'E',
                        _ => panic!("Not happening"),
                    },
                    90 => match direction {
                        'N' => direction = 'E',
                        'S' => direction = 'W',
                        'E' => direction = 'S',
                        'W' => direction = 'N',
                        _ => panic!("Not happening"),
                    },
                    _ => panic!("Unsupported direction {}{}", instruction.directive, instruction.value)
                }
            }
            'F' => match direction {
                'N' => y += instruction.value,
                'S' => y -= instruction.value,
                'E' => x += instruction.value,
                'W' => x -= instruction.value,
                _ => panic!("Not happening"),
            },
            _ => panic!("Unknown directive {}", instruction.directive)
        }
    }

    let result = x.abs() + y.abs();
    println!("{}", result);
    return result;
}

#[derive(Debug)]
struct Instruction {
    directive: char,
    value: i32
}

fn parse_instruction(line: &String) -> Instruction {
    Instruction {
        directive: line.chars().nth(0).unwrap(),
        value: line[1..line.len()].parse::<i32>().unwrap()
    }
}

fn solve_second(values: &Vec::<String>) -> i32 {
    let mut x = 10;
    let mut y = 1;
    let mut ship_x: i32 = 0;
    let mut ship_y: i32 = 0;

    for line in values {
        let instruction = parse_instruction(&line);
        match instruction.directive {
            'N' => y += instruction.value,
            'S' => y -= instruction.value,
            'E' => x += instruction.value,
            'W' => x -= instruction.value,
            'L' => {
                // Axis rotation
                let theta: f32 = ((360 - instruction.value) as f32) * PI / 180.0;
                let new_x = x * (theta.cos() as i32) + y * (theta.sin() as i32);
                y = -x * (theta.sin() as i32) + y * (theta.cos() as i32);
                x = new_x;
            },
            'R' => {
                // Axis rotation
                let theta: f32 = (instruction.value as f32) * PI / 180.0;
                let new_x = x * (theta.cos() as i32) + y * (theta.sin() as i32);
                y = -x * (theta.sin() as i32) + y * (theta.cos() as i32);
                x = new_x;
            }
            'F' => {
                ship_x += x * instruction.value;
                ship_y += y * instruction.value;
            },
            _ => panic!("Unknown directive {}", instruction.directive)
        }

        // println!("Ship: ({} ,{})", ship_x, ship_y);
        // println!("Waypoint: ({} ,{})", x, y);
    }

    let result = ship_x.abs() + ship_y.abs();
    println!("{}", result);
    return result;
}

fn main() {
    let values = load_values(String::from("input.txt"));
    let _result_first = solve_first(&values);
    assert_eq!(_result_first, 1687);
    let _result_second = solve_second(&values);
    assert_eq!(_result_second, 20873);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sample() {
        let values = load_values(String::from("sample.txt"));
        let expected1 = 25;
        assert_eq!(solve_first(&values), expected1);
        let expected2 = 286;
        assert_eq!(solve_second(&values), expected2);
    }
}

