def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 0
    assert part2(sample) == 0

    puzzle = loadfile("input.txt")
    print(part1(puzzle))
    print(part2(puzzle))


def part1(data):
    """ Solve Part 1"""
    return 0


def part2(data):
    """ Solve Part 2"""
    return 0


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
