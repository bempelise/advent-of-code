import argparse
import os
import pathlib
import shutil
import subprocess
import shlex


from contextlib import contextmanager


def main():
    """ main """
    parser = argparse.ArgumentParser(description='make day for advent.')
    parser.add_argument('-d', '--day', type=int, required=True,
                        help='day of advent')
    parser.add_argument('--year', type=int, default=2024,
                        help='year of advent')

    args = parser.parse_args()

    create(args.year, args.day)


def create(year, day):
    """ creates directory and files for year and day """
    create_dir(str(year), delete=False)

    with change_dir(str(year)):
        daydir = "day" + str(day)
        if year == 2021:    # Python year
            create_dir(daydir)
            with change_dir(daydir):
                open('input.txt', 'x').close()
                open('sample.txt', 'x').close()
                shutil.copyfile(f"../../template.py", f"day{day}.py")
        elif year == 2020:  # Rust year
            print(os.getcwd())
            cmd = f"cargo new {daydir}"
            subprocess.run(shlex.split(cmd))
            shutil.copy2(f"../template.rs", os.path.join(daydir, "src", "main.rs"))
        elif year == 2024: # Go year
            create_dir(daydir)
            with change_dir(daydir):
                open('input.txt', 'x').close()
                open('sample.txt', 'x').close()
                shutil.copyfile(f"../../template.go", f"day{day}.go")



@contextmanager
def change_dir(directory):
    """ Changes context to the given directory
        then changes back to current on return
    """
    prev = os.getcwd()
    try:
        yield os.chdir(directory)
    finally:
        os.chdir(prev)


def create_dir(directory, delete=True):
    """ Prepares the output directory"""

    if os.path.isdir(directory) and delete:
        shutil.rmtree(directory)

    if not os.path.isdir(directory):
        pathlib.Path(directory).mkdir(parents=True, exist_ok=True)


if __name__ == "__main__":
    main()
