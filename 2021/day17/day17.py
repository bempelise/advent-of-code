from math import sqrt, ceil


def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 45
    assert part2(sample) == 112

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 12561
    assert part2(puzzle) == 3785


def part1(data):
    """ Solve Part 1"""
    target = get_area(data)

    v0y = abs(target.minp.y) - 1
    ymax = v0y*v0y - v0y * (v0y - 1)/2

    return int(ymax)


def part2(data):
    """ Solve Part 2"""
    target = get_area(data)
    vx_min = ceil(sqrt(2 * target.minp.x + 0.25) - 0.5)
    vx_max = target.maxp.x + 1
    vy_min = target.minp.y
    vy_max = abs(target.minp.y)

    count = 0

    for i in range(vx_min, vx_max):
        for j in range(vy_min, vy_max):
            success = simulate(Point(i, j), target)
            if success:
                count += 1
    return count


def simulate(velocity, target):
    current = Point(0, 0)

    while True:
        current.x += velocity.x
        current.y += velocity.y

        if target.contains(current):
            return True

        if current.past(target):
            return False

        if velocity.x != 0:
            if velocity.x > 0:
                velocity.x -= 1
            else:
                velocity.x += 1

        velocity.y -= 1


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def past(self, area):
        return self.x > area.maxp.x or self.y < area.minp.y

    def __str__(self):
        return str(self.x) + ", " + str(self.y)


class Area(object):
    def __init__(self, minp, maxp):
        self.minp = minp
        self.maxp = maxp

    def contains(self, point):
        return (
                    point.x >= self.minp.x and point.x <= self.maxp.x
                    and
                    point.y >= self.minp.y and point.y <= self.maxp.y
               )


def get_area(data):
    line = data[0]
    line = line.replace("target area: ", "")
    coords = line.split(',')
    xcoords = coords[0].split('=')[1]
    ycoords = coords[1].split('=')[1]
    xpoints = xcoords.split("..")
    ypoints = ycoords.split("..")
    return Area(Point(int(xpoints[0]), int(ypoints[0])), Point(int(xpoints[1]), int(ypoints[1])))


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
