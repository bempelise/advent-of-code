def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 1656
    assert part2(sample) == 195

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 1640
    assert part2(puzzle) == 312


def part1(data):
    """ Solve Part 1"""
    matrix = get_matrix(data)
    flashes = 0

    for _ in range(100):
        # print_matrix(matrix)
        day_flashes, _ = run_day(matrix)
        flashes += day_flashes

    return flashes


def run_day(matrix):
    """ run a single day """
    flashes = 0
    whole = False
    for i, line in enumerate(matrix):
        for j in range(len(line)):
            increase(Point(i, j, matrix[i][j]), matrix)

    for i, line in enumerate(matrix):
        for j in range(len(line)):
            if matrix[i][j] > 9:
                flashes += 1
                matrix[i][j] = 0

    whole = flashes == len(matrix) * len(matrix[0])

    return flashes, whole


def part2(data):
    """ Solve Part 2"""
    matrix = get_matrix(data)

    index = 0
    whole = False

    while not whole:
        index += 1
        _, whole = run_day(matrix)

    return index


class Point(object):
    def __init__(self, x, y, value):
        self.x = x
        self.y = y
        self.value = value


def increase(point, matrix):
    matrix[point.x][point.y] += 1

    if matrix[point.x][point.y] == 10:
        neighbours = get_neighbours(point, matrix)
        for neighbour in neighbours:
            increase(neighbour, matrix)


def get_neighbours(point, matrix):
    width = len(matrix)
    height = len(matrix[0])

    neighbours = []

    # cross
    if point.x > 0:
        neighbours.append(Point(point.x - 1, point.y, matrix[point.x - 1][point.y]))
    if point.y > 0:
        neighbours.append(Point(point.x, point.y - 1, matrix[point.x][point.y - 1]))
    if point.x < width - 1:
        neighbours.append(Point(point.x + 1, point.y, matrix[point.x + 1][point.y]))
    if point.y < height - 1:
        neighbours.append(Point(point.x, point.y + 1, matrix[point.x][point.y + 1]))
    # diag
    if point.x > 0 and point.y > 0:
        neighbours.append(Point(point.x - 1, point.y - 1, matrix[point.x - 1][point.y - 1]))
    if point.x < width - 1 and point.y > 0:
        neighbours.append(Point(point.x + 1, point.y - 1, matrix[point.x + 1][point.y - 1]))
    if point.x > 0 and point.y < height - 1:
        neighbours.append(Point(point.x - 1, point.y + 1, matrix[point.x - 1][point.y + 1]))
    if point.x < width - 1 and point.y < height - 1:
        neighbours.append(Point(point.x + 1, point.y + 1, matrix[point.x + 1][point.y + 1]))

    return neighbours


def get_matrix(data):
    """ get matrix """
    matrix = []
    for line in data:
        matrix_line = []
        for c in line:
            matrix_line.append(int(c))
        matrix.append(matrix_line)
    return matrix


def print_matrix(matrix):
    """ print matrix """
    matrix_str = ""
    for line in matrix:
        for octopus in line:
            matrix_str += str(octopus)
        matrix_str += "\n"
    print(matrix_str)


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
