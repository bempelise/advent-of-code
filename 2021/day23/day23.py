from heapq import heappop, heappush

ENERGY = { 'A': 1,
           'B': 10,
           'C': 100,
           'D': 1000 }
STEPOUT = { 'A':2,
            'B':4,
            'C':6,
            'D':8 }
PARK = [0, 1, 3, 5, 7, 9, 10]
ROOMS = { 'A': [11, 15],
          'B': [12, 16],
          'C': [13, 17],
          'D': [14, 18] }

ROOMS2 = { 'A': [11, 15, 19, 23],
           'B': [12, 16, 20, 24],
           'C': [13, 17, 21, 25],
           'D': [14, 18, 22, 26] }

INVERSE = { v:key for key,val in ROOMS.items() for v in val }
INVERSE2 = { v:key for key,val in ROOMS2.items() for v in val }

def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 12521
    sample = loadfile("sample2.0.txt")
    assert part2(sample) == 44169

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 10321
    puzzle = loadfile("input2.txt")
    assert part2(puzzle) == 46451


def enter_room(state, rooms, inverse):
    for corr_pos in parked(state):
        amphi = state[corr_pos]
        best_pos = None
        for room_pos in rooms[amphi]:
            if state[room_pos] == ".":
                best_pos = room_pos
            elif state[room_pos] != amphi:
                best_pos = None
                break

        if best_pos is not None and is_possible(state, corr_pos, STEPOUT[inverse[best_pos]]):
            yield corr_pos, best_pos


def leave_room(state, rooms):
    for amphi in 'ABCD':
        for room_pos in rooms[amphi]:
            if state[room_pos] == ".":
                continue
            else:
                for corr_pos in available(state):
                    if is_possible(state, STEPOUT[amphi], corr_pos):
                        yield room_pos, corr_pos
                break


def available(state):
    for pos in PARK:
        if state[pos] == ".":
            yield pos

def parked(state):
    for pos in PARK:
        if state[pos] != ".":
            yield pos

def possible_moves(state, inverse):
    rooms = ROOMS if len(state) < 20 else ROOMS2
    for src, dst in enter_room(state, rooms, inverse):
        yield src, dst

    for src, dst in leave_room(state, rooms):
        yield src, dst



def can_enter_room(a,b,amphi,puzzle,room_pos):
  for pos in room_pos:
    if puzzle[pos] == '.': best_pos = pos
    elif puzzle[pos] != amphi: return False
  if not blocked(a,b,puzzle): return best_pos

def blocked(a,b,puzzle):
  step = 1 if a<b else -1
  for pos in range(a+step, b+step, step):
    if puzzle[pos] != '.': return True




def is_possible(state, src, dst):
    step = 1 if src < dst else -1

    for i in range(src + step, dst + step, step):
        if state[i] != ".":
            return False
    return True


def distance(state, src, dst, inverse):
    if src > dst:
        src, dst = dst, src
    return abs(STEPOUT[inverse[dst]] - src) + (dst - 7)//4


def move(state, src, dst):
    new_state = list(state)
    new_state[src], new_state[dst] = state[dst],  state[src]
    return "".join(new_state)


def part1(data, target = '.'*11 + 'ABCD'*2):
    """ Solve Part 1"""
    origin = "".join(data).replace("#", "")
    inverse = INVERSE if len(origin) < 20 else INVERSE2

    print(f"{origin} -> {target}")

    heap = [(0, origin)]
    seen = { origin : 0 }
    while heap:
        cost, state = heappop(heap)

        # print(f"({cost}) {state}")

        if state == target:
            return cost

        for src, dst in possible_moves(state, inverse):
            move_cost = distance(state, src, dst, inverse) * ENERGY[state[src]]
            new_state = move(state, src, dst)

            cached = seen.get(new_state)
            if cached != None and cached <= cost + move_cost:
                continue

            seen[new_state] = cost + move_cost

            # print(f"-> ({cost + move_cost}) {new_state}")

            heappush(heap, (seen[new_state], new_state))


def part2(data, target = '.'*11 + 'ABCD'*4):
    """ Solve Part 2"""
    target = "".join(target).replace("#", "")
    return part1(data, target)


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data




if __name__ == "__main__":
    main()


