from math import ceil, floor


def main():
    """ main """

    verify_explode()
    verify_magnitude()
    verify_add()

    sample = loadfile("sample.txt")
    verify_parse(sample)
    assert str(add_all(sample)) == "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
    assert part1(sample) == 3488

    sample1 = loadfile("sample1.txt")
    verify_parse(sample1)
    assert str(add_all(sample1)) == "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"
    assert part1(sample1) == 4140
    assert part2(sample1) == 3993

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 4072
    assert part2(puzzle) == 4483


def part1(data):
    """ Solve Part 1"""
    node = add_all(data)
    return node.magnitude()


def part2(data):
    """ Solve Part 2"""

    magnitude = 0

    for i in range(len(data)):
        for j in range(len(data)):
            if i != j:
                current = add_all([data[i], data[j]])
                current.reduce()
                current_mag = current.magnitude()
                if current_mag > magnitude:
                    magnitude = current_mag
    return magnitude



def add_all(data):
    """ adds all nodes in given data """
    node = None
    for line in data:
        node = add_nodes(node, parse_node(line))
        node.reduce()
    return node

class Node(object):
    def __init__(self, left, right):
        self.parent = None
        self.left = left
        self.right = right

        if isinstance(self.left, Node):
            self.left.parent = self
        if isinstance(self.right, Node):
            self.right.parent = self

    def __str__(self):
        return "[" + str(self.left) + "," + str(self.right) + "]"

    def reduce(self):
        while True:
            # print(self)
            if self.explode():
                continue
            if self.split():
                continue
            break

    def explode(self, depth=0):
        if self.is_leaf():
            if depth < 4:
                return False
            self.increase_next_right(self.right)
            self.increase_next_left(self.left)
            if self.parent.left is self:
                self.parent.left = 0
            else:
                self.parent.right = 0
            return True

        if isinstance(self.left, Node):
            if self.left.explode(depth + 1):
                return True
        if isinstance(self.right, Node):
            if self.right.explode(depth + 1):
                return True

        return False


    def split(self):
        if isinstance(self.left, Node):
            if self.left.split():
                return True
        else:
            if self.left > 9:
                self.left = Node(floor(self.left/2), ceil(self.left/2))
                self.left.parent = self
                return True

        if isinstance(self.right, Node):
            if self.right.split():
                return True
        else:
            if self.right > 9:
                self.right = Node(floor(self.right/2), ceil(self.right/2))
                self.right.parent = self
                return True
        return False

    def is_leaf(self):
        """ return true if both children are literals"""
        return not isinstance(self.left, Node) and not isinstance(self.right, Node)

    def increase_next_right(self, value):
        """ increase next right literal if any"""
        if self.parent is None:
            return

        if self.parent.right is not self:
            if not isinstance(self.parent.right, Node):
                self.parent.right += value
                return

            current = self.parent.right
            while isinstance(current.left, Node):
                current = current.left
            current.left += value
        else:
            self.parent.increase_next_right(value)

    def increase_next_left(self, value):
        """ increase next left literal if any"""
        if self.parent is None:
            return

        if self.parent.left is not self:
            if not isinstance(self.parent.left, Node):
                self.parent.left += value
                return

            current = self.parent.left
            while isinstance(current.right, Node):
                current = current.right
            current.right += value
        else:
            self.parent.increase_next_left(value)

    def magnitude(self):
        magnitude = 0
        if isinstance(self.left, Node):
            magnitude += 3 * self.left.magnitude()
        else:
            magnitude += 3 * self.left

        if isinstance(self.right, Node):
            magnitude += 2 * self.right.magnitude()
        else:
            magnitude += 2 * self.right

        return magnitude


def parse_node(line):
    opened = []
    right = []
    left = []

    for c in line:
        if c == "[":
            opened.append(c)
        elif c == ",":
            opened.append(c)
        elif c == "]":
            node = Node(left.pop(), right.pop())
            opened.pop()
            opened.pop()
            if not opened:
                return node
            if opened[-1] == "[":
                left.append(node)
            else:
                right.append(node)
        else:
            if opened[-1] == "[":
                left.append(int(c))
            else:
                right.append(int(c))


def add_nodes(left, right):
    if left is None:
        return right
    if right is None:
        return left
    return Node(left, right)


def verify_parse(data):
    for line in data:
        assert str(parse_node(line)) == line


def verify_explode():
    node = parse_node("[[[[[9,8],1],2],3],4]")
    node.explode()
    assert str(node) == "[[[[0,9],2],3],4]"

    node = parse_node("[7,[6,[5,[4,[3,2]]]]]")
    node.explode()
    assert str(node) == "[7,[6,[5,[7,0]]]]"

    node = parse_node("[[6,[5,[4,[3,2]]]],1]")
    node.explode()
    assert str(node) == "[[6,[5,[7,0]]],3]"

    node = parse_node("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]")
    node.explode()
    assert str(node) == "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"

    node = parse_node("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]")
    node.explode()
    assert str(node) == "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"


def verify_magnitude():
    assert parse_node("[9,1]").magnitude() == 29
    assert parse_node("[1,9]").magnitude() == 21
    assert parse_node("[[9,1],[1,9]]").magnitude() == 129
    assert parse_node("[[1,2],[[3,4],5]]").magnitude() == 143
    assert parse_node("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]").magnitude() == 1384
    assert parse_node("[[[[1,1],[2,2]],[3,3]],[4,4]]").magnitude() == 445
    assert parse_node("[[[[3,0],[5,3]],[4,4]],[5,5]]").magnitude() == 791
    assert parse_node("[[[[5,0],[7,4]],[5,5]],[6,6]]").magnitude() == 1137
    assert parse_node("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]").magnitude() == 3488

def verify_add():
    nodes = ["[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]",
             "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]"]
    assert str(add_all(nodes)) == "[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]"
    nodes = ["[1,1]", "[2,2]", "[3,3]", "[4,4]"]
    assert str(add_all(nodes)) == "[[[[1,1],[2,2]],[3,3]],[4,4]]"
    nodes = ["[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]"]
    assert str(add_all(nodes)) == "[[[[3,0],[5,3]],[4,4]],[5,5]]"
    nodes = ["[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]"]
    assert str(add_all(nodes)) == "[[[[5,0],[7,4]],[5,5]],[6,6]]"
    nodes = ["[[[[4,3],4],4],[7,[[8,4],9]]]", "[1,1]"]
    assert str(add_all(nodes)) == "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"




def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
