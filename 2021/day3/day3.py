def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 198
    assert part2(sample) == 230

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 3277364
    assert part2(puzzle) == 5736383


def part1(data):
    """ Solve Part 1"""
    return solve_gamma_epsilon(data)


def part2(data):
    """ Solve Part 2"""
    return solve_oxygen_co2(data)


def solve_gamma_epsilon(data):
    """ finds gamma & epsilon """
    buckets = get_buckets(data)
    gamma = get_binary(buckets)
    epsilon = ~gamma & bitmask(len(buckets))

    return epsilon * gamma


def solve_oxygen_co2(data):
    """ finds oxygen & co2 """
    buckets = get_buckets(data)
    oxygen = get_oxygen(data, buckets[0], 0)
    co2 = get_co2(data, buckets[0], 0)
    return oxygen * co2


def get_oxygen(data, bucket, index):
    """ returns oxygen """
    filtered = list(filter(lambda value: int(value[index]) == bucket, data))
    if len(filtered) == 1:
        return get_binary(filtered[0])

    return get_oxygen(filtered, get_bucket(filtered, index + 1), index + 1)


def get_co2(data, bucket, index):
    """ returns co2 """
    bucket = (bucket + 1) % 2
    filtered = list(filter(lambda value: int(value[index]) == bucket, data))
    if len(filtered) == 1:
        return get_binary(filtered[0])

    return get_co2(filtered, get_bucket(filtered, index + 1), index + 1)


def get_bucket(data, index):
    """ gets bucket at index """
    bucket = 0
    for line in data:
        if line[index] == '1':
            bucket += 1
        else:
            bucket -= 1
    return bucket2bin(bucket)


def get_buckets(data):
    """ returns buckets for each column """
    buckets = [0] * (len(data[0]))

    for line in data:
        for index, _ in enumerate(buckets):
            if line[index] == '1':
                buckets[index] += 1
            else:
                buckets[index] -= 1
    return list(map(bucket2bin, buckets))


def bucket2bin(value):
    """ returns 1 if positive 0 otherwise """
    if value >= 0:
        return 1
    return 0


def get_binary(buckets):
    """ gets binary value out of buckets"""
    binary = 0
    index = len(buckets) - 1
    for value in buckets:
        binary += pow(2, index) * int(value)
        index -= 1
    return binary


def bitmask(length):
    """ returns bitmask of given length """
    return (1 << (length - 1)) - 1


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
