def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 15
    assert part2(sample) == 1134

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 562
    assert part2(puzzle) == 1076922


def part1(data):
    """ Solve Part 1"""
    hmap = get_hmap(data)
    risk_level = 0

    width = len(hmap)
    height = len(hmap[0])

    for i in range(width):
        for j in range(height):
            neighbours = get_neighbours(Point(i, j, hmap[i][j]), hmap)
            is_low_point = True
            for neighbour in neighbours:
                if neighbour.value <= hmap[i][j]:
                    is_low_point = False

            if is_low_point:
                risk_level += hmap[i][j] + 1

    return risk_level


class Point(object):
    def __init__(self, x, y, value):
        self.x = x
        self.y = y
        self.value = value


def get_neighbours(point, hmap):
    width = len(hmap)
    height = len(hmap[0])

    neighbours = []
    if point.x > 0:
        neighbours.append(Point(point.x - 1, point.y, hmap[point.x - 1][point.y]))
    if point.y > 0:
        neighbours.append(Point(point.x, point.y - 1, hmap[point.x][point.y - 1]))
    if point.x < width - 1:
        neighbours.append(Point(point.x + 1, point.y, hmap[point.x + 1][point.y]))
    if point.y < height - 1:
        neighbours.append(Point(point.x, point.y + 1, hmap[point.x][point.y + 1]))

    return neighbours


def part2(data):
    """ Solve Part 2"""
    hmap = get_hmap(data)

    basins = []
    width = len(hmap)
    height = len(hmap[0])

    for i in range(width):
        for j in range(height):
            neighbours = get_neighbours(Point(i, j, hmap[i][j]), hmap)
            is_low_point = True
            for neighbour in neighbours:
                if neighbour.value <= hmap[i][j]:
                    is_low_point = False

            if is_low_point:
                basin = set()
                get_basin(Point(i, j, hmap[i][j]), hmap, basin)
                basins.append(len(basin))

    basins = sorted(basins, reverse=True)

    return basins[0] * basins[1] * basins[2]


def get_basin(point, hmap, basin):
    """ returns size of basin on given point """
    basin.add(point.x*100 + point.y)

    neighbours = get_neighbours(point, hmap)
    for neighbour in neighbours:
        if neighbour.value > point.value and neighbour.value < 9:
            get_basin(neighbour, hmap, basin)


def get_hmap(data):
    hmap = []
    for line in data:
        hline = []
        for c in line:
            hline.append(int(c))
        hmap.append(hline)
    return hmap


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
