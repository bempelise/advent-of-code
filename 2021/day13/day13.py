def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 17
    assert part2(sample) == 0

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 706
    print(part2(puzzle))


def part1(data):
    """ Solve Part 1"""
    sheet, folds = get_sheet(data)
    sheet = fold_sheet(sheet, folds[0])
    return dots(sheet)


def part2(data):
    """ Solve Part 2"""
    sheet, folds = get_sheet(data)
    for fold in folds:
        sheet = fold_sheet(sheet, fold)

    print()
    print_sheet(sheet)
    print()
    return 0


def get_sheet(data):
    points = []
    folds = []
    max_x = None
    max_y = None

    for line in data:
        if not line:
            continue

        if line.startswith("fold along"):
            folds.append(line.replace("fold along", "").strip())
            continue

        points.append(get_point(line))
        if max_x is None or points[-1][0] > max_x:
            max_x = points[-1][0]
        if max_y is None or points[-1][1] > max_y:
            max_y = points[-1][1]

    sheet = []
    for _ in range(max_y + 1):
        sheet.append(['.'] * (max_x + 1))

    for point in points:
        sheet[point[1]][point[0]] = '#'

    return sheet, folds


def fold_sheet(sheet, fold):
    """ folds sheet along the fold """
    axis = fold.split('=')[0]
    pos = int(fold.split('=')[1])

    if axis == 'y':
        return fold_y(sheet, pos)
    return fold_x(sheet, pos)


def fold_y(sheet, pos):
    """ fold y """
    print(pos)
    folded = []
    for line in range(pos):
        folded.append(sheet[line].copy())

    print_sheet(folded)

    for y in range(len(sheet) - pos):
        orig_y = y + pos + 1
        folded_y = len(folded) - y - 1
        if folded_y < 0 or orig_y >= len(sheet):
            break

        for x in range(len(folded[0])):
            folded[folded_y][x] = fold(folded[folded_y][x], sheet[orig_y][x])

    return folded


def fold_x(sheet, pos):
    """ fold x """
    folded = []
    for line in sheet:
        newline = []
        for entry in range(pos):
            newline.append(line[entry])
        folded.append(newline)


    for y in range(len(folded)):
        for x in range(len(sheet[0]) - pos):
            orig_x = x + pos + 1
            folded_x = len(folded[0]) - x - 1

            if folded_x < 0 or orig_x >= len(sheet[0]):
                break

            folded[y][folded_x] = fold(folded[y][folded_x], sheet[y][orig_x])

    return folded


def fold(first, second):
    """ folds dots of on paper """
    if first == '.' and second == '.':
        return "."
    return '#'


def get_point(line):
    """ get point """
    return list(map(int, line.split(",")))


def print_sheet(sheet):
    """ print sheet """
    sheet_str = ""
    for i, line in enumerate(sheet):
        for j in range(len(line)):
            sheet_str += str(sheet[i][j])
        sheet_str += "\n"
    print(sheet_str)


def dots(sheet):
    """ counts dots on sheet """
    count = 0
    for i, line in enumerate(sheet):
        for j in range(len(line)):
            if sheet[i][j] == '#':
                count += 1
    return count


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
