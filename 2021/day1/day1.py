def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 7
    assert part2(sample) == 5

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 1167
    assert part2(puzzle) == 1130


def part1(data):
    """ Solve Part 1"""
    return solve(data, 1)


def part2(data):
    """ Solve Part 2"""
    return solve(data, 3)


def solve(data, window_size):
    prev = None
    count = 0
    window = []

    for line in data:
        window.append(int(line))
        if len(window) < window_size:
            continue

        current = window_sum(window)

        if prev is not None:
            if current > prev:
                count += 1
        prev = current

        window.pop(0)

    return count


def window_sum(window):
    """ window sum """
    count = 0
    for i in window:
        count += i
    return count


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
