from collections import Counter

def main():
    """ main """
    # sample = loadfile("sample.txt")
    sample = (4, 8)
    assert part1(sample) == 739785
    assert part2(sample) == 444356092776315

    # puzzle = loadfile("input.txt")
    puzzle = (7, 8)
    assert part1(puzzle) == 556206
    assert part2(puzzle) == 630797200227453


def part1(data):
    """ Solve Part 1"""
    player1 = Player(data[0])
    player2 = Player(data[1])
    game = Game(player1, player2)
    game.simulate()
    return game.answer()


def part2(data):
    """ Solve Part 2"""
    game = DiracGame(data[0], data[1])
    game.simulate()
    return max(game.player1wins, game.player2wins)


class Player(object):
    def __init__(self, pos, score=0, limit=1000):
        self.pos = pos
        self.score = score
        self.limit = limit

    def has_won(self):
        return self.score >= self.limit

    def moveby(self, spaces):
        self.pos += spaces
        while self.pos > 10:
            self.pos -= 10
        self.score += self.pos


class Game(object):
    def __init__(self, player1, player2):
        self.players = []
        self.players.append(player1)
        self.players.append(player2)
        self.die = 1
        self.rolls = 0
        self.winner = None

    def simulate(self):
        """ simulate a game"""
        done = False
        while not done:
            for index, player in enumerate(self.players):
                player.moveby(self.roll(3))
                if player.has_won():
                    done = True
                    self.winner = index
                    break

        print("Game finished after {} rolls with Player {} winner with {} points".format(
              self.rolls,
              self.winner + 1,
              self.players[self.winner].score))

    def answer(self):
        for player in self.players:
            if player.has_won():
                continue
            return player.score * self.rolls

    def roll(self, amount):
        move = 0
        for _ in range(amount):
            move += self.die
            self.increase_die()
        return move

    def increase_die(self):
        self.rolls += 1
        self.die = self.die + 1
        if self.die > 100:
            self.die = 1


class DiracGame(object):
    def __init__(self, player1, player2):
        self.outcomes = Counter()
        self.outcomes[((player1, 0), (player2, 0))] = 1
        self.player1wins = 0
        self.player2wins = 0

    def simulate(self):
        while self.outcomes:
            self.move_player1()
            self.move_player2()

    def move_player1(self):
        """ Move Player1 """
        new_outcomes = Counter()
        for outcome, count in self.outcomes.items():
            new_outcomes[(get_pos_score(outcome[0], 3), outcome[1])] = count * 1 + new_outcomes[(get_pos_score(outcome[0], 3), outcome[1])]
            new_outcomes[(get_pos_score(outcome[0], 4), outcome[1])] = count * 3 + new_outcomes[(get_pos_score(outcome[0], 4), outcome[1])]
            new_outcomes[(get_pos_score(outcome[0], 5), outcome[1])] = count * 6 + new_outcomes[(get_pos_score(outcome[0], 5), outcome[1])]
            new_outcomes[(get_pos_score(outcome[0], 6), outcome[1])] = count * 7 + new_outcomes[(get_pos_score(outcome[0], 6), outcome[1])]
            new_outcomes[(get_pos_score(outcome[0], 7), outcome[1])] = count * 6 + new_outcomes[(get_pos_score(outcome[0], 7), outcome[1])]
            new_outcomes[(get_pos_score(outcome[0], 8), outcome[1])] = count * 3 + new_outcomes[(get_pos_score(outcome[0], 8), outcome[1])]
            new_outcomes[(get_pos_score(outcome[0], 9), outcome[1])] = count * 1 + new_outcomes[(get_pos_score(outcome[0], 9), outcome[1])]

        to_delete = []
        for outcome, count in new_outcomes.items():
            if outcome[0][1] >= 21:
                to_delete.append(outcome)
                self.player1wins += count

        for entry in to_delete:
            new_outcomes.pop(entry)

        self.outcomes = new_outcomes

    def move_player2(self):
        """ Move Player2 """
        new_outcomes = Counter()
        for outcome, count in self.outcomes.items():
            new_outcomes[(outcome[0], get_pos_score(outcome[1], 3))] = count * 1 + new_outcomes[(outcome[0], get_pos_score(outcome[1], 3))]
            new_outcomes[(outcome[0], get_pos_score(outcome[1], 4))] = count * 3 + new_outcomes[(outcome[0], get_pos_score(outcome[1], 4))]
            new_outcomes[(outcome[0], get_pos_score(outcome[1], 5))] = count * 6 + new_outcomes[(outcome[0], get_pos_score(outcome[1], 5))]
            new_outcomes[(outcome[0], get_pos_score(outcome[1], 6))] = count * 7 + new_outcomes[(outcome[0], get_pos_score(outcome[1], 6))]
            new_outcomes[(outcome[0], get_pos_score(outcome[1], 7))] = count * 6 + new_outcomes[(outcome[0], get_pos_score(outcome[1], 7))]
            new_outcomes[(outcome[0], get_pos_score(outcome[1], 8))] = count * 3 + new_outcomes[(outcome[0], get_pos_score(outcome[1], 8))]
            new_outcomes[(outcome[0], get_pos_score(outcome[1], 9))] = count * 1 + new_outcomes[(outcome[0], get_pos_score(outcome[1], 9))]

        to_delete = []
        for outcome, count in new_outcomes.items():
            if outcome[1][1] >= 21:
                to_delete.append(outcome)
                self.player2wins += count

        for entry in to_delete:
            new_outcomes.pop(entry)

        self.outcomes = new_outcomes


def get_pos_score(pos_score, spaces):
    pos = pos_score[0]
    score = pos_score[1]

    pos += spaces
    while pos > 10:
        pos -= 10
    score += pos

    return (pos, score)


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
