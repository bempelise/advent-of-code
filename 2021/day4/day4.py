def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 4512
    assert part2(sample) == 1924

    puzzle = loadfile("input.txt")
    print(part1(puzzle))    # 82440
    print(part2(puzzle))    # 20774


def part1(data):
    """ Solve Part 1"""
    numbers, bingos = get_game(data)

    for number in numbers:
        for bingo in bingos:
            if bingo.add(number):
                return bingo.score()
    return 0


def part2(data):
    """ Solve Part 2"""
    numbers, bingos = get_game(data)

    wins = 0

    for number in numbers:
        for bingo in bingos:
            if bingo.add(number):
                wins += 1
                if wins == len(bingos):
                    return bingo.score()
    return 0


class Bingo(object):
    """ Bingo board """
    def __init__(self, lines):
        assert len(lines) == len(lines[0])

        self.lines = lines
        self.size = len(self.lines)

        self.marks = []
        for _ in range(self.size):
            self.marks.append([0] * self.size)

        self.last = None
        self.win = False

    def add(self, number):
        """ add game number to bingo board """
        if self.win:
            return False

        self.last = number
        for i, line in enumerate(self.lines):
            for j, entry in enumerate(line):
                if entry == number:
                    self.marks[i][j] = 1
                    self.check_win(i, j)
                    if self.win:
                        return True
                    return False
        return False

    def check_win(self, i, j):
        """ check if board wins """
        # Check row
        count = 0
        for index in range(self.size):
            count += self.marks[index][j]
        if count == self.size:
            self.win = True
            return

        # check column
        count = 0
        for index in range(self.size):
            count += self.marks[i][index]
        if count == self.size:
            self.win = True

        # # check diag1:
        # count = 0
        # for index in range(self.size):
        #     for jndex in range(self.size):
        #         if index == jndex:
        #             count += self.marks[index][jndex]
        # if count == self.size:
        #     print("diag")
        #     self.win = True

        # # check diag2:
        # count = 0
        # for index in range(self.size):
        #     for jndex in range(self.size):
        #         if index + jndex == self.size - 1:
        #             count += self.marks[index][jndex]
        # if count == self.size:
        #     self.win = True

    def score(self):
        """ bingo board score """
        if not self.win:
            return 0

        count = 0
        for index in range(self.size):
            for jndex in range(self.size):
                if self.marks[index][jndex] == 0:
                    count += self.lines[index][jndex]

        return count * self.last


def get_game(data):
    """ gets game setup """
    bingos = []

    lines = []

    for index, line in enumerate(data):
        if index == 0:
            numbers = list(map(int, line.split(',')))
            continue
        if not line:
            if lines:
                bingos.append(Bingo(lines.copy()))
                lines.clear()
            continue

        lines.append(list(map(int, line.split())))

    if lines:
        bingos.append(Bingo(lines.copy()))
        lines.clear()

    return numbers, bingos




def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
