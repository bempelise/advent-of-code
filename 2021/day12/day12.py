def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 10
    assert part2(sample) == 36

    sample = loadfile("sample2.txt")
    assert part1(sample) == 19
    assert part2(sample) == 103

    sample = loadfile("sample3.txt")
    assert part1(sample) == 226
    assert part2(sample) == 3509

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 4749
    assert part2(puzzle) == 123054


def part1(data):
    """ Solve Part 1"""

    nodes = get_graph(data)

    start = get_node("start", nodes)

    path = []
    paths = start.visit(path)
    return len(paths)


def part2(data):
    """ Solve Part 2"""
    nodes = get_graph(data)

    start = get_node("start", nodes)

    path = []
    paths = start.visit2(path, False)
    return len(paths)


class Node(object):
    def __init__(self, name):
        self.name = name
        self.is_small = self.name[0].islower()
        self.neighbours = []

    def connect(self, node):
        self.neighbours.append(node)
        node.neighbours.append(self)

    def visit(self, path):
        paths = []
        path.append(self)

        if self.name == "end":
            paths.append(path)
        else:
            for neighbour in self.neighbours:
                if (neighbour.is_small and neighbour not in path) or not neighbour.is_small:
                    new_paths = neighbour.visit(path.copy())
                    if new_paths:
                        paths.extend(new_paths)
        return paths

    def visit2(self, path, has_visited_twice):
        paths = []
        path.append(self)

        if self.name == "end":
            paths.append(path)
        else:
            for neighbour in self.neighbours:
                if neighbour.name == "start":
                    continue

                new_paths = []

                if (neighbour.is_small and neighbour not in path) or not neighbour.is_small:
                    new_paths = neighbour.visit2(path.copy(), has_visited_twice)
                elif not has_visited_twice:
                    new_paths = neighbour.visit2(path.copy(), True)

                if new_paths:
                    paths.extend(new_paths)
        return paths


def print_paths(paths):
    """ prints paths """
    for path in paths:
        print_path(path)


def print_path(path):
    """ prints path """
    path_str = ""
    for index, node in enumerate(path):
        path_str += node.name
        if index < len(path) - 1:
            path_str += "->"
    print(path_str)


def get_graph(data):
    """ get graph of caverns """
    nodes = []

    for line in data:
        connection = line.split('-')
        source = get_node(connection[0], nodes)
        if source is None:
            source = Node(connection[0])
            nodes.append(source)

        sink = get_node(connection[1], nodes)
        if sink is None:
            sink = Node(connection[1])
            nodes.append(sink)
        source.connect(sink)

    return nodes


def get_node(name, nodes):
    for node in nodes:
        if node.name == name:
            return node
    return None


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
