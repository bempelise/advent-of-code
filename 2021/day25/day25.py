def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 58
    assert part2(sample) == 0

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 560
    # print(part2(puzzle))


def part1(data):
    """ Solve Part 1"""
    matrix = Matrix(data)
    count = 0

    while True:
        moves = matrix.next()
        count += 1
        print(f"{count}: {moves}" )
        if moves == 0:
            break
    return count


def part2(data):
    """ Solve Part 2"""
    return 0


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data

class Matrix():

    def __init__(self, data):
        self.height = len(data)
        self.width = len(data[0])

        self.east = {}
        self.south = {}

        for index, line in enumerate(data):
            for subindex, c in enumerate(line):
                if c == ".":
                    pass
                elif c == ">":
                    if not index in self.east:
                        self.east[index] = [subindex]
                    else:
                        self.east[index].append(subindex)
                elif c == "v":
                    if not index in self.south:
                        self.south[index] = [subindex]
                    else:
                        self.south[index].append(subindex)

    def next(self):
        moves = self.move_east()
        moves += self.move_south()
        return moves

    def move_east(self):
        moves = 0
        new_east = {}
        for line, entries in self.east.items():
            new_east[line] = []
            for entry in entries:
                if entry + 1 < self.width:
                    next_entry = entry + 1
                else:
                    next_entry = 0

                if (line in self.east and next_entry in self.east[line]) or (line in self.south and next_entry in self.south[line]):
                    new_east[line].append(entry)
                    continue
                else:
                    new_east[line].append(next_entry)
                    moves += 1

        self.east = new_east
        return moves


    def move_south(self):
        moves = 0
        new_south = {}

        for line, entries in self.south.items():
            if line + 1 < self.height:
                next_line = line + 1
            else:
                next_line = 0

            for entry in entries:

                if (next_line in self.south and entry in self.south[next_line]) or (next_line in self.east and entry in self.east[next_line]):
                    if line in new_south:
                        new_south[line].append(entry)
                    else:
                        new_south[line] = [entry]
                    continue
                else:
                    if next_line in new_south:
                        new_south[next_line].append(entry)
                    else:
                        new_south[next_line] = [entry]
                        # print(f"{next_line}, {entry}")
                    moves += 1

        self.south = new_south
        return moves

    def print(self):

        for i in range(self.height):
            line = ""
            for j in range(self.width):
                if i in self.east and j in self.east[i]:
                    line += ">"
                elif i in self.south and j in self.south[i]:
                    line += "v"
                else:
                    line += "."

            print(line)

    def print_state(self):
        print_matrix(self.south)
        print_matrix(self.east)

def print_matrix(matrix):
    for key in sorted(matrix.keys()):
        entry = sorted(matrix[key])
        print(f"{key}: {entry}")





if __name__ == "__main__":
    main()
