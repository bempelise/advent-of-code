def main():
    """ main """
    assert part1(["D2FE28"]) == 6
    assert part1(["38006F45291200"]) == 9
    assert part1(["EE00D40C823060"]) == 14
    assert part1(["8A004A801A8002F478"]) == 16
    assert part1(["620080001611562C8802118E34"]) == 12
    assert part1(["C0015000016115A2E0802F182340"]) == 23
    assert part1(["A0016C880162017C3686B18A3D4780"]) == 31

    # assert part2(sample) == 0

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 969
    assert part2(puzzle) == 124921618408


def part1(data):
    """ Solve Part 1"""
    numbers = get_numbers(data)
    version_sum, _, _ = parse_packet(numbers[0], 0)
    return version_sum


def parse_packet(number, offset):
    version, offset = get_value(number, 3, offset)
    type_id, offset = get_value(number, 3, offset)

    if type_id == 4:    # literal
        literal, offset = get_literal(number, offset)
        return version, literal, offset

    length_type_id, offset = get_value(number, 1, offset)

    operands = []

    if length_type_id == 0:
        length, offset = get_value(number, 15, offset)
        start_offset = offset
        while offset - start_offset < length:
            subversion, literal, offset = parse_packet(number, offset)
            operands.append(literal)
            version += subversion
    else:
        subpackets, offset = get_value(number, 11, offset)
        for _ in range(subpackets):
            subversion, literal, offset = parse_packet(number, offset)
            operands.append(literal)
            version += subversion

    if type_id == 0:    # sum
        literal = 0
        for operand in operands:
            literal += operand
    if type_id == 1:    # product
        if len(operands) == 0:
            literal = 0
        else:
            literal = 1
            for operand in operands:
                literal *= operand
    if type_id == 2:    # minimum
        literal = None
        for operand in operands:
            if literal is None or operand < literal:
                literal = operand
    if type_id == 3:    # maximum
        literal = None
        for operand in operands:
            if literal is None or operand > literal:
                literal = operand
    if type_id == 5:    # greater than
        assert len(operands) == 2
        if operands[0] > operands[1]:
            literal = 1
        else:
            literal = 0
    if type_id == 6:    # less than
        assert len(operands) == 2
        if operands[0] < operands[1]:
            literal = 1
        else:
            literal = 0
    if type_id == 7:    # equal to
        assert len(operands) == 2
        if operands[0] == operands[1]:
            literal = 1
        else:
            literal = 0

    return version, literal, offset


def part2(data):
    """ Solve Part 2"""
    numbers = get_numbers(data)
    _, literal, _ = parse_packet(numbers[0], 0)
    return literal


def get_literal(number, offset):
    has_more = True
    literal = 0
    while has_more:
        literal = literal << 4
        next_value, offset = get_value(number, 5, offset)
        has_more, _ = get_value((next_value, 5), 1, 0)
        next_literal, _ = get_value((next_value, 5), 4, 1)
        literal += next_literal
    # print("Literal: {}".format(literal))
    return literal, offset


def get_numbers(data):
    numbers = []
    for line in data:
        numbers.append((int("0x" + line, 0), len(line) * 4))
    return numbers

def get_value(number, size, offset):
    mask = get_mask(size)
    shift = number[1] - offset - mask.bit_length()
    bit_mask = mask << shift
    value = (number[0] & bit_mask) >> shift
    offset += size
    return value, offset


def get_mask(size):
    sign_mask = 1 << size
    return sign_mask - 1


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
