def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample, 18) == 26
    assert part1(sample, 80) == 5934
    assert part2(sample) == 26984457539

    puzzle = loadfile("input.txt")
    assert part1(puzzle, 80) == 380243
    assert part2(puzzle) == 1708791884591


class Bucket(object):
    def __init__(self, day, fish=0):
        self.day = day
        self.fish = fish


def part1(data, days):
    """ Solve Part 1"""
    lanternfish = get_fish(data)
    buckets = []

    for i in range(6):
        buckets.append(Bucket(i))

    for fish in lanternfish:
        buckets[fish].fish += 1

    for _ in range(days):
        new_fish = 0
        for bucket in buckets:
            if bucket.day == 0:
                new_fish += bucket.fish
                bucket.day = 6
            else:
                bucket.day -= 1

        buckets.append(Bucket(8, new_fish))

    count = 0
    for bucket in buckets:
        count += bucket.fish
    return count


def part2(data):
    """ Solve Part 2"""
    return part1(data, 256)


def get_fish(data):
    """ gets the initial fish from input """
    return list(map(int, data[0].split(",")))


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
