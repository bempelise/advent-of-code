from math import sqrt
from collections import Counter


def main():
    """ main """
    sample1 = loadfile("sample1.txt")
    assert part1(sample1) == 3
    assert part2(sample1) == 7

    sample = loadfile("sample.txt")
    assert part1(sample) == 79
    assert part2(sample) == 3621

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 449
    assert part2(puzzle) == 13128


def part1(data):
    """ Solve Part 1"""
    scanners = get_scanners(data)

    for scanner in scanners:
        scanner.prepare()

    scanners = correlate_scanners(scanners)

    points = Counter()
    for scanner in scanners:
        for point in scanner.points:
            points[(point.x, point.y, point.z)] += 1

    return len(points.keys())


def part2(data):
    """ Solve Part 2"""
    scanners = get_scanners(data)

    for scanner in scanners:
        scanner.prepare()

    scanners = correlate_scanners(scanners)

    distances = []

    for i, scanner in enumerate(scanners):
        for j, other in enumerate(scanners):
            if i != j:
                distances.append(scanner.position.manhattan_distance(other.position))

    return max(distances)


def correlate_scanners(scanners):
    """ correlate scanners to find positions """
    scanners[0].normalized = True
    normalized = [scanners[0].count]
    count = 0

    while len(normalized) < len(scanners):
        for index, scanner in enumerate(scanners):
            if index in normalized:
                continue
            scanners[normalized[count]].correlate(scanner)
            if scanner.normalized:
                normalized.append(scanner.count)
        count += 1
    return scanners

class Point(object):
    def __init__(self, x, y, z=0):
        self.x = x
        self.y = y
        self.z = z

    def euclidean_distance(self, other):
        return sqrt(pow(self.x - other.x, 2) + pow(self.y - other.y, 2) + pow(self.z - other.z, 2))

    def manhattan_distance(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y) + abs(self.z - other.z)

    def add(self, other):
        return Point(self.x + other.x, self.y + other.y, self.z + other.z)

    def diff(self, other):
        return Point(self.x - other.x, self.y - other.y, self.z - other.z)

    def mult(self, other):
        return Point(self.x * other.x, self.y * other.y, self.z * other.z)

    def shift(self):
        return Point(self.y, self.z, self.x)

    def transpose(self):
        return Point(self.x, self.z, self.y)

    def __str__(self):
        string = str(self.x) + "," + str(self.y)
        if self.z != 0:
            string += "," + str(self.z)
        return string


class Scanner(object):
    def __init__(self, count):
        self.count = count
        self.points = []
        self.distances = Counter()
        self.normalized = False
        self.position = Point(0, 0, 0)

    def add(self, point):
        self.points.append(point)

    def prepare(self):
        self.distances.clear()
        for index, point in enumerate(self.points):
            for jndex, other in enumerate(self.points):
                if index != jndex:
                    distance = point.euclidean_distance(other)
                    if distance < 500:
                        self.distances[point] += distance

    def correlate(self, other):
        pairs = []

        for point, distance in self.distances.items():
            for other_point, other_distance in other.distances.items():
                if distance == other_distance:
                    pairs.append((point, other_point))

        if len(pairs) < 3:
            # print("not enough pairs")
            return

        skew, shift, transpose = get_transform(pairs)
        if skew is None:
            # print("no skew")
            return
        drift = get_drift(pairs, skew)

        other.normalize(skew, drift, shift, transpose)

    def normalize(self, skew, drift, shift, transpose):
        # transpose_str = ""
        # if transpose:
        #     transpose_str = "><"
        # print("Correlated scanner {} [{}] - [{}] << {} {}".format(self.count, drift, skew, shift, transpose_str))

        for i, _ in enumerate(self.points):
            if transpose:
                self.points[i] = self.points[i].transpose()

            for _ in range(shift):
                self.points[i] = self.points[i].shift()

            self.points[i] = self.points[i].mult(skew).add(drift)

        self.prepare()
        self.normalized = True
        self.position = drift

        # print(self)

    def __str__(self):
        string = "Scanner {}\n".format(self.count)
        for point in self.points:
            string += str(point) + "\n"
        return string


def get_drift(pairs, skew):
    drifts = Counter()

    for pair in pairs:
        drift = pair[0].diff(pair[1].mult(skew))
        drifts[(drift.x, drift.y, drift.z)] += 1

    if len(drifts.keys()) > 1:
        return None
    key = list(drifts.keys())[0]
    drift = Point(key[0], key[1], key[2])
    return drift


def get_transform(pairs):
    shift = 0
    transpose = False

    skew = get_skew(pairs)
    if skew is not None:
        return skew, shift, transpose

    shift += 1
    for i, pair in enumerate(pairs):
        pairs[i] = (pair[0], pair[1].shift())

    skew = get_skew(pairs)
    if skew is not None:
        return skew, shift, transpose

    shift += 1
    for i, pair in enumerate(pairs):
        pairs[i] = (pair[0], pair[1].shift())

    skew = get_skew(pairs)
    if skew is not None:
        return skew, shift, transpose

    shift = 0
    transpose = True
    for i, pair in enumerate(pairs):
        pairs[i] = (pair[0], pair[1].shift().transpose())

    skew = get_skew(pairs)
    if skew is not None:
        return skew, shift, transpose

    shift += 1
    for i, pair in enumerate(pairs):
        pairs[i] = (pair[0], pair[1].shift())

    skew = get_skew(pairs)
    if skew is not None:
        return skew, shift, transpose

    shift += 1
    for i, pair in enumerate(pairs):
        pairs[i] = (pair[0], pair[1].shift())

    skew = get_skew(pairs)
    if skew is not None:
        return skew, shift, transpose

    return None, None, None


def get_skew(pairs):
    skew = Point(1, 1, 1)

    diffs = set()
    for pair in pairs:
        diffs.add(pair[0].diff(pair[1].mult(skew)).x)

    if len(diffs) > 1:
        skew.x = -1

    diffs = set()
    for pair in pairs:
        diffs.add(pair[0].diff(pair[1].mult(skew)).x)

    if len(diffs) > 1:
        return None

    diffs = set()
    for pair in pairs:
        diffs.add(pair[0].diff(pair[1].mult(skew)).y)

    if len(diffs) > 1:
        skew.y = -1

    diffs = set()
    for pair in pairs:
        diffs.add(pair[0].diff(pair[1].mult(skew)).y)

    if len(diffs) > 1:
        return None

    diffs = set()
    for pair in pairs:
        diffs.add(pair[0].diff(pair[1].mult(skew)).z)

    if len(diffs) > 1:
        skew.z = -1

    diffs = set()
    for pair in pairs:
        diffs.add(pair[0].diff(pair[1].mult(skew)).z)

    if len(diffs) > 1:
        return None

    return skew



def get_scanners(data):
    scanners = []
    count = 0
    scanner = None

    for line in data:
        if not line:
            continue

        if line.startswith("---"):
            if scanner is not None:
                scanners.append(scanner)
            scanner = Scanner(count)
            count += 1
            continue

        points = line.split(",")
        if len(points) == 2:
            scanner.add(Point(int(points[0]), int(points[1])))
        else:
            scanner.add(Point(int(points[0]), int(points[1]), int(points[2])))
    scanners.append(scanner)

    return scanners


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
