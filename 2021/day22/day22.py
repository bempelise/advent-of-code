def main():
    """ main """
    sample = loadfile("sample1.txt")
    assert part1(sample) == 39

    sample = loadfile("sample.txt")
    assert part1(sample) == 590784

    sample = loadfile("sample2.txt")
    assert part1(sample) == 474140
    assert part2(sample) == 2758514936282235

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 546724
    assert part2(puzzle) == 1346544039176841


def part1(data):
    """ Solve Part 1"""
    steps = get_steps(data)
    init_segment = Segment(-50, 50)
    init_region = Cuboid(init_segment, init_segment, init_segment, False)
    return execute_steps(steps, init_region)


def part2(data):
    """ Solve Part 2"""
    steps = get_steps(data)
    return execute_steps(steps)


def execute_steps(steps, region=None):
    cuboids = []
    for step in steps:
        if region is not None:
            change = region.merge(step)
        else:
            change = step
        if change is None:
            continue
        if step.status:
            add(cuboids, change)
            # cuboids.append(change)
        else:
            compliments = []
            todelete = []
            for cuboid in cuboids:
                if not cuboid.status:
                    todelete.append(cuboid)
                    continue

                compliment = cuboid.compliment(change)
                if cuboid.merge(change) is not None:
                    compliments.extend(compliment)
                    todelete.append(cuboid)

            for cuboid in todelete:
                cuboids.remove(cuboid)

            for compliment in compliments:
                add(cuboids, compliment)

        # lit(cuboids)
    return lit(cuboids)


def lit(cuboids):
    totalsize = 0
    for cuboid in cuboids:
        totalsize += cuboid.size()
    return totalsize


class Segment(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def overlap(self, other):
        if self.start > other.end or other.start > self.end:
            return None
        return Segment(max(self.start, other.start), min(self.end, other.end))

    def size(self):
        return self.end - self.start + 1

    def __str__(self):
        return "(" + str(self.start) + "," + str(self.end) + ")"


class Cuboid(object):
    def __init__(self, x, y, z, status):
        self.x = x
        self.y = y
        self.z = z
        self.status = status
        self.pts = None

    def merge(self, o):
        mergex = self.x.overlap(o.x)
        if mergex is None:
            return None
        mergey = self.y.overlap(o.y)
        if mergey is None:
            return None
        mergez = self.z.overlap(o.z)
        if mergez is None:
            return None
        return Cuboid(Segment(mergex.start, mergex.end),
                      Segment(mergey.start, mergey.end),
                      Segment(mergez.start, mergez.end),
                      o.status)

    def compliment(self, o):
        compl = []
        append(compl, self.x.start, o.x.start - 1, self.y.start, o.y.start - 1, self.z.start, o.z.start - 1, self.status)
        append(compl, self.x.start, o.x.start - 1, max(self.y.start, o.y.start), min(self.y.end, o.y.end), self.z.start, o.z.start - 1, self.status)
        append(compl, self.x.start, o.x.start - 1, o.y.end + 1, self.y.end, self.z.start, o.z.start - 1, self.status)
        append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), self.y.start, o.y.start - 1, self.z.start, o.z.start - 1, self.status)
        append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), max(self.y.start, o.y.start), min(self.y.end, o.y.end), self.z.start, o.z.start - 1, self.status)
        append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), o.y.end + 1, self.y.end, self.z.start, o.z.start - 1, self.status)
        append(compl, o.x.end + 1, self.x.end, self.y.start, o.y.start - 1, self.z.start, o.z.start - 1, self.status)
        append(compl, o.x.end + 1, self.x.end, max(self.y.start, o.y.start), min(self.y.end, o.y.end), self.z.start, o.z.start - 1, self.status)
        append(compl, o.x.end + 1, self.x.end, o.y.end + 1, self.y.end, self.z.start, o.z.start - 1, self.status)

        append(compl, self.x.start, o.x.start - 1, self.y.start, o.y.start - 1, max(self.z.start, o.z.start), min(self.z.end, o.z.end), self.status)
        append(compl, self.x.start, o.x.start - 1, max(self.y.start, o.y.start), min(self.y.end, o.y.end), max(self.z.start, o.z.start), min(self.z.end, o.z.end), self.status)
        append(compl, self.x.start, o.x.start - 1, o.y.end + 1, self.y.end, max(self.z.start, o.z.start), min(self.z.end, o.z.end), self.status)
        append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), self.y.start, o.y.start - 1, max(self.z.start, o.z.start), min(self.z.end, o.z.end), self.status)
        # append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), max(self.y.start, o.y.start), min(self.y.end, o.y.end), max(self.z.start, o.z.start), min(self.z.end, o.z.end), o.status)
        append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), o.y.end + 1, self.y.end, max(self.z.start, o.z.start), min(self.z.end, o.z.end), self.status)
        append(compl, o.x.end + 1, self.x.end, self.y.start, o.y.start - 1, max(self.z.start, o.z.start), min(self.z.end, o.z.end), self.status)
        append(compl, o.x.end + 1, self.x.end, max(self.y.start, o.y.start), min(self.y.end, o.y.end), max(self.z.start, o.z.start), min(self.z.end, o.z.end), self.status)
        append(compl, o.x.end + 1, self.x.end, o.y.end + 1, self.y.end, max(self.z.start, o.z.start), min(self.z.end, o.z.end), self.status)

        append(compl, self.x.start, o.x.start - 1, self.y.start, o.y.start - 1, o.z.end + 1, self.z.end, self.status)
        append(compl, self.x.start, o.x.start - 1, max(self.y.start, o.y.start), min(self.y.end, o.y.end), o.z.end + 1, self.z.end, self.status)
        append(compl, self.x.start, o.x.start - 1, o.y.end + 1, self.y.end, o.z.end + 1, self.z.end, self.status)
        append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), self.y.start, o.y.start - 1, o.z.end + 1, self.z.end, self.status)
        append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), max(self.y.start, o.y.start), min(self.y.end, o.y.end), o.z.end + 1, self.z.end, self.status)
        append(compl, max(self.x.start, o.x.start), min(self.x.end, o.x.end), o.y.end + 1, self.y.end, o.z.end + 1, self.z.end, self.status)
        append(compl, o.x.end + 1, self.x.end, self.y.start, o.y.start - 1, o.z.end + 1, self.z.end, self.status)
        append(compl, o.x.end + 1, self.x.end, max(self.y.start, o.y.start), min(self.y.end, o.y.end), o.z.end + 1, self.z.end, self.status)
        append(compl, o.x.end + 1, self.x.end, o.y.end + 1, self.y.end, o.z.end + 1, self.z.end, self.status)
        return compl

    def size(self):
        return self.x.size() * self.y.size() * self.z.size()

    def __str__(self):
        return str(self.x) + str(self.y) + str(self.z) + "(" + str(self.status) + ")"


def append(cuboids, startx, endx, starty, endy, startz, endz, status):
    if startx > endx:
        return
    if starty > endy:
        return
    if startz > endz:
        return
    cuboids.append(Cuboid(Segment(startx, endx),
                          Segment(starty, endy),
                          Segment(startz, endz),
                          status))


def get_steps(data):
    steps = []
    for line in data:
        split1 = line.split(" ")
        status_str = split1[0]
        status = status_str == "on"
        split2 = split1[1].split(",")
        xcoords = split2[0].split("=")[1].split('..')
        ycoords = split2[1].split("=")[1].split('..')
        zcoords = split2[2].split("=")[1].split('..')
        steps.append(Cuboid(Segment(int(xcoords[0]), int(xcoords[1])),
                            Segment(int(ycoords[0]), int(ycoords[1])),
                            Segment(int(zcoords[0]), int(zcoords[1])),
                            status))
    return steps


def add(cuboids, other):
    toadd = [other]
    for cuboid in cuboids:
        toadd = update_compls(cuboid, toadd)
    cuboids.extend(toadd)


def update_compls(cuboid, toadd):
    todelete = []
    compls = []
    for entry in toadd:
        if entry.merge(cuboid):
            todelete.append(entry)
            compls.extend(entry.compliment(cuboid))

    for entry in todelete:
        toadd.remove(entry)
    toadd.extend(compls)
    return toadd


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
