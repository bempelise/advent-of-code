def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 35
    assert part2(sample, "sample.dump") == 3351

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 4964
    print(part2(puzzle, "input.dump"))


def part1(data):
    """ Solve Part 1"""
    algo, image = get_algoimage(data)
    framesize = 10
    image = enhance(image, algo, framesize)
    image = enhance(image, algo, framesize)

    if algo[0] == '#':
        image = remove_frame(image, framesize)

    count = 0
    for line in image:
        for c in line:
            if c == '#':
                count += 1
    return count


def part2(data, filename):
    """ Solve Part 2"""
    algo, image = get_algoimage(data)
    framesize = 10
    for _ in range(25):
        image = enhance(image, algo, framesize)
        image = enhance(image, algo, framesize)
        if algo[0] == '#':
            image = remove_frame(image, framesize)

    dump_image(image, filename)

    count = 0
    for line in image:
        for c in line:
            if c == '#':
                count += 1
    return count


def enhance(image, algo, framesize):
    framed = []

    for _ in range(framesize):
        framed.append(''.join(['.'] * (len(image[0]) + 2*framesize)))

    for line in image:
        framed.append("."*framesize + line + "."*framesize)

    for _ in range(framesize):
        framed.append(''.join(['.'] * (len(image[0]) + 2*framesize)))

    outputlist = []
    for _ in range(len(framed)):
        outputlist.append(['.'] * (len(image[0]) + 2*framesize))

    for i, line in enumerate(framed):
        if i in [0, len(framed) - 1]:
            continue

        for j, _ in enumerate(line):
            if j in [0, len(line) - 1]:
                continue

            value = 0
            value += get_value(framed[i+1][j+1], 0)
            value += get_value(framed[i+1][j], 1)
            value += get_value(framed[i+1][j-1], 2)
            value += get_value(framed[i][j+1], 3)
            value += get_value(framed[i][j], 4)
            value += get_value(framed[i][j-1], 5)
            value += get_value(framed[i-1][j+1], 6)
            value += get_value(framed[i-1][j], 7)
            value += get_value(framed[i-1][j-1], 8)
            outputlist[i][j] = algo[value]

    output = []
    for line in outputlist:
        output.append(''.join(line))

    return output


def remove_frame(image, framesize):
    output = []
    framesize += 2
    for i in range(framesize, len(image) - framesize):
        output.append(image[i][framesize: len(image[i]) - framesize])
    return output


def get_value(pixel, shift):
    if pixel == '#':
        return 1 << shift
    if pixel == '.':
        return 0


def get_algoimage(data):
    image = []
    for i, line in enumerate(data):
        if i == 0:
            algo = line
            continue
        if not line:
            continue
        image.append(line)
    return algo, image


def print_image(image):
    for line in image:
        print(line)
    print()


def dump_image(image, filename):
    with open(filename, 'w') as file:
        for line in image:
            file.write(line + "\n")


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
