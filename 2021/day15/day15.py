from queue import PriorityQueue

def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 40
    assert part2(sample) == 315

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 702
    print(part2(puzzle))


def part1(data):
    """ Solve Part 1"""
    matrix = get_matrix(data)
    # print_matrix(matrix)
    start = matrix[0][0]
    end = matrix[len(matrix) - 1][len(matrix[0]) - 1]
    came_from = a_star(matrix, start, end)
    cost, path = get_path(came_from, start, end)
    # print_matrix(matrix, path)
    return cost


def part2(data):
    """ Solve Part 2"""
    matrix = get_matrix(data)
    matrix = expand_matrix(matrix)
    print_matrix(matrix)
    start = matrix[0][0]
    end = matrix[len(matrix) - 1][len(matrix[0]) - 1]
    came_from = a_star(matrix, start, end)
    cost, path = get_path(came_from, start, end)
    print_matrix(matrix, path)
    return cost


def get_path(came_from, start, end):
    current = end
    cost = 0
    path = [current]
    while current != start:
        cost += current.cost
        current = came_from[current]
        path = [current] + path

    # print_path(path)

    return cost, path


def print_path(path):
    path_str = ""
    for i, entry in enumerate(path):
        path_str += "(" + str(entry.x) + ", " + str(entry.y) + ")"
        if i < len(path) - 1:
            path_str += " -> "
    print(path_str)
    print()


def print_matrix(matrix, path=None):
    matrix_str = ""
    for i, line in enumerate(matrix):
        for j in range(len(line)):
            if path is None or matrix[i][j] in path:
                if matrix[i][j] is None:
                    print(str(i) + ", " + str(j))
                    matrix_str += "#"
                matrix_str += str(matrix[i][j].cost)
            else:
                matrix_str += "."
        matrix_str += "\n"
    print(matrix_str)


def a_star(matrix, start, end):
    frontier = PriorityQueue()
    came_from = dict()
    cost_so_far = dict()

    frontier.put((0, start))
    came_from[start] = None
    cost_so_far[start] = start.cost

    while not frontier.empty():
        current_tuple = frontier.get()
        current = current_tuple[1]

        if current == end:
            return came_from

        neighbours = get_neighbours(current, matrix)
        for neigh in neighbours:
            new_cost = cost_so_far[current] + neigh.cost
            if neigh not in cost_so_far or new_cost < cost_so_far[neigh]:
                cost_so_far[neigh] = new_cost
                priority = new_cost + heuristic(matrix, neigh, end)

                new_item = (priority, neigh)
                frontier.put(new_item)
                came_from[neigh] = current

    return None


def heuristic(matrix, start, end):
    # Manhattan distance on a square grid
    return abs(start.x - end.x) + abs(start.y - end.y)


def get_neighbours(point, matrix):
    width = len(matrix)
    height = len(matrix[0])

    neighbours = []

    # cross
    if point.x > 0:
        neighbours.append(matrix[point.x - 1][point.y])
    if point.y > 0:
        neighbours.append(matrix[point.x][point.y - 1])
    if point.x < width - 1:
        neighbours.append(matrix[point.x + 1][point.y])
    if point.y < height - 1:
        neighbours.append(matrix[point.x][point.y + 1])

    return neighbours


class Point(object):
    def __init__(self, x, y, cost):
        self.x = x
        self.y = y
        self.cost = cost

    def __lt__(self, other):
        if self.x > other.x:
            return self
        if self.y > other.y:
            return self
        return other


def get_matrix(data):
    matrix = []
    for i, line in enumerate(data):
        mline = []
        for j, c in enumerate(line):
            mline.append(Point(i, j, int(c)))
        matrix.append(mline)
    return matrix


def expand_matrix(matrix, expansion=5):
    expanded = []
    for i in range(len(matrix) * expansion):
        eline = [None] * (len(matrix[0]) * expansion)
        expanded.append(eline)

    for i, line in enumerate(matrix):
        for j, point in enumerate(line):
            for k in range(expansion):
                for l in range(expansion):
                    cost = point.cost + k + l
                    if cost > 9:
                        cost = cost % 9
                    expanded[i + l*len(line)][j + k*len(line)] = Point(i + l*len(line), j + k*len(line), cost)

    return expanded


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
