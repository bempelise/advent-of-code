def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 150
    assert part2(sample) == 900

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 1451208
    assert part2(puzzle) == 1620141160


def part1(data):
    """ Solve Part 1"""
    return solve_depth(data)


def part2(data):
    """ Solve Part 2"""
    return solve_aim(data)


def solve_depth(data):
    horizontal = 0
    depth = 0

    for line in data:
        command, change = get_command(line)

        if command == "forward":
            horizontal += change
        elif command == "down":
            depth += change
        elif command == "up":
            depth -= change

    return horizontal*depth


def solve_aim(data):
    horizontal = 0
    depth = 0
    aim = 0

    for line in data:
        command, change = get_command(line)

        if command == "forward":
            horizontal += change
            depth += aim*change
        elif command == "down":
            aim += change
        elif command == "up":
            aim -= change

    return horizontal*depth


def get_command(line):
    tokens = line.split(' ')
    return tokens[0], int(tokens[1])


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
