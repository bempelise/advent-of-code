from collections import Counter

def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 1588
    assert part2(sample) == 2188189693529

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 2590
    print(part2(puzzle))


def part1(data):
    """ Solve Part 1"""
    polymer, _, rules = get_polymer_rules(data)
    rules = update_rules(rules, 5)
    rules = update_rules(rules, 2)
    polymer = polymerize(polymer, rules, 1)
    low, high = get_low_high(polymer)
    return high - low


def part2(data):
    """ Solve Part 2"""
    polymer, rules, _ = get_polymer_rules(data)

    count = Counter()

    for i in range(len(polymer) - 1):
        key = polymer[i] + polymer[i + 1]
        count[key] += 1

    for _ in range(40):
        tcount = Counter()
        for key, value in count.items():
            if key in rules:
                tcount[key[0] + rules[key]] += value
                tcount[rules[key] + key[1]] += value
        count = tcount

    fcount = Counter()
    for key, value in count.items():
        fcount[key[1]] += value

    return max(fcount.values()) - min(fcount.values())


def update_rules(rules, times):
    newrules = {}
    for key in rules:
        newrules[key] = polymerize(key, rules, times)
    return newrules

def polymerize(polymer, rules, times):
    if times == 0:
        return polymer

    new_polymer = []
    for i in range(len(polymer) - 1):
        key = polymer[i] + polymer[i + 1]

        if key in rules.keys():
            if new_polymer:
                new_polymer.pop()
            new_polymer.extend(polymerize(rules[key], rules, times - 1))

    return "".join(new_polymer)


def get_low_high(polymer):
    buckets = {}
    for c in polymer:
        if c not in buckets.keys():
            buckets[c] = 1
        else:
            buckets[c] += 1

    low = None
    high = None

    for value in buckets.values():
        if low is None or value < low:
            low = value
        if high is None or value > high:
            high = value
    return low, high


def apply_rules(polymer, rules):
    insertions = []
    for i, c in enumerate(polymer):
        if i < len(polymer) - 1:
            key = c + polymer[i + 1]
            if key in rules.keys():
                insertions.append(rules[key])
            else:
                insertions.append(None)

    assert len(insertions) == len(polymer) - 1

    new_polymer = polymer[0]
    for index in range(len(insertions)):
        new_polymer += insertions[index] + polymer[index + 1]

    return new_polymer


def get_polymer_rules(data):
    rules = {}
    advrules = {}
    polymer = []
    for index, line in enumerate(data):
        if index == 0:
            polymer.extend(line)
            continue

        if not line:
            continue

        rule = line.split("->")
        rules[rule[0].strip()] = rule[1].strip()
        advrules[rule[0].strip()] = rule[0][0].strip() + rule[1].strip() + rule[0][1].strip()
    return polymer, rules, advrules


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
