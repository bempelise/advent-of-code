def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 26
    assert part2(sample) == 61229

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 330
    assert part2(puzzle) == 1010472


def part1(data):
    """ Solve Part 1"""
    _, numbers = parse(data)

    count = 0
    for line in numbers:
        for number in line:
            if len(number) in [2, 3, 4, 7]:
                count += 1

    return count


def part2(data):
    """ Solve Part 2"""
    digits, numbers = parse(data)

    count = 0
    for digit, number in zip(digits, numbers):
        coding = get_coding(digit)
        count += decode(coding, number)

    return count


def get_coding(digits):
    """ deduces the digits encoding"""

    # get 1, 4, 7, 8 and indexes
    index1 = None
    index4 = None
    index7 = None
    index8 = None
    index2_3_5 = []
    index0_6_9 = []
    for index, digit in enumerate(digits):
        if len(digit) == 2:
            index1 = index
        elif len(digit) == 3:
            index7 = index
        elif len(digit) == 4:
            index4 = index
        elif len(digit) == 5:
            index2_3_5.append(index)
        elif len(digit) == 6:
            index0_6_9.append(index)
        elif len(digit) == 7:
            index8 = index

    # get 0, 5
    index0 = None
    index5 = None
    tpl = None
    mid = None
    tpl_and_mid = []

    for digit in digits[index4]:
        if digit not in digits[index1]:
            tpl_and_mid.append(digit)

    for index in index2_3_5:
        if tpl_and_mid[0] in digits[index] and tpl_and_mid[1] in digits[index]:
            index5 = index

    for index in index2_3_5:
        if index == index5:
            continue

        for digit in tpl_and_mid:
            if digit not in digits[index]:
                tpl = digit

    for digit in tpl_and_mid:
        if digit != tpl:
            mid = digit

    for index in index0_6_9:
        if mid not in digits[index]:
            index0 = index

    # get 2, 3
    index2 = None
    index3 = None
    top = None
    btr_and_bot = []

    for digit in digits[index7]:
        if digit not in digits[index1]:
            top = digit

    for digit in digits[index5]:
        if digit != top and digit not in tpl_and_mid:
            btr_and_bot.append(digit)

    for index in index2_3_5:
        if index == index5:
            continue

        if btr_and_bot[0] in digits[index] and btr_and_bot[1] in digits[index]:
            index3 = index
        else:
            index2 = index

    # get 6, 9
    index6 = None
    index9 = None

    for index in index0_6_9:
        if index == index0:
            continue

        if digits[index1][0] in digits[index] and digits[index1][1] in digits[index]:
            index9 = index
        else:
            index6 = index

    coding = {}
    coding[''.join(sorted(digits[index0]))] = 0
    coding[''.join(sorted(digits[index1]))] = 1
    coding[''.join(sorted(digits[index2]))] = 2
    coding[''.join(sorted(digits[index3]))] = 3
    coding[''.join(sorted(digits[index4]))] = 4
    coding[''.join(sorted(digits[index5]))] = 5
    coding[''.join(sorted(digits[index6]))] = 6
    coding[''.join(sorted(digits[index7]))] = 7
    coding[''.join(sorted(digits[index8]))] = 8
    coding[''.join(sorted(digits[index9]))] = 9

    return coding


def decode(coding, number):
    """ decodes number using encoding"""
    size = len(number) - 1
    value = 0
    for index, digit in enumerate(number):
        value += coding[''.join(sorted(digit))] * pow(10, size - index)
    return value


def parse(data):
    """ parses puzzle data """
    digits = []
    numbers = []
    for line in data:
        line_digits, line_numbers = parse_line(line)
        digits.append(line_digits)
        numbers.append(line_numbers)

    return digits, numbers


def parse_line(line):
    """ parses single line of data """
    inputs = line.split('|')
    digits = inputs[0].split()
    numbers = inputs[1].split()
    return digits, numbers


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
