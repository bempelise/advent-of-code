    shift                 adjust    offset
1   1                       11        5
2       1                   13        5
3           1               12        1
4               1           15        15
5                   1       10        2
6                   26      -1        2
7                   1       14        5
8                   26      -8        8
9               26          -7        14
10          26              -8        12
11          1               11        7
12          26              -2        14
13      26                  -2        13
14  26                      -13       6

W1 - W14    W1 + 5 - 13 = W14
W2 - W13    W2 + 5 - 2  = W13
W3 - W10    W3 + 1 - 8  = W10
W4 - W9     W4 + 15 - 7 = W9
W5 - W6     W5 + 2 - 1  = W6
W7 - W8     W7 + 5 - 8 = W8
W11 = W12   W11 + 7 - 2 = W12

Largest: 96918996924991
Smallest: 91811241911641

