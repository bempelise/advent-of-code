import math

def main():
    """ main """
    # commands = loadfile("sample1.txt")
    # assert part1(commands) == 0
    # commands = loadfile("sample2.txt")
    # assert part1(commands) == 0
    # commands = loadfile("sample3.txt")
    # assert part1(commands) == 0

    # assert part2(commands) == 0

    puzzle = loadfile("input.txt")
    part1(puzzle)
    # print(part2(puzzle))


def part1(commands):
    """ Solve Part 1"""
    memory = { "z": 0 }

    try:
        for command in commands:
            command.execute(memory)
    except KeyError as ex:
        print(f"Error on {ex}")
        return 1

    print("Final memory")
    for variable, value in memory.items():
        print(f"{variable}: {value}")
    print()

    return 0


def part2(commands):
    """ Solve Part 2"""
    return 0


def loadfile(filename):
    """ Load file """
    commands = []
    with open(filename, 'r') as file:
        for line in file.readlines():
            commands.append(Command(line.strip().split(" ")))
    return commands

class Command():
    """ ALU command """
    def __init__(self, operands):
        self.operation = operands[0]
        self.destination = operands[1]
        if len(operands) > 2:
            self.operand = operands[2]

    def execute(self, memory):
        if self.operation == "inp":
            print(f"z: {memory['z']}")
            memory[self.destination] = int(input(f"{self.destination}: "))
        else:
            if is_number(self.operand):
                value = int(self.operand)
            elif not self.operand in memory:
                value = 0
            else:
                value = memory[self.operand]

            if not self.destination in memory:
                memory[self.destination] = 0


            if self.operation == "add":
                memory[self.destination] = memory[self.destination] + value
            elif self.operation == "mul":
                memory[self.destination] = int(memory[self.destination] * value)
            elif self.operation == "div":
                memory[self.destination] = math.floor(memory[self.destination] / value)
            elif self.operation == "mod":
                memory[self.destination] = memory[self.destination] % value
            elif self.operation == "eql":
                if memory[self.destination] == value:
                    memory[self.destination] = 1
                else:
                    memory[self.destination] = 0


def is_number(value):
    """ Is Number """
    try:
        int_value = int(value)
    except ValueError:
        return False
    return True

if __name__ == "__main__":
    main()
