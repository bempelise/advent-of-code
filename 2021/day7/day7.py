def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 37
    assert part2(sample) == 168

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 342534
    assert part2(puzzle) == 94004208


def part1(data):
    """ Solve Part 1"""
    positions = get_positions(data)
    average = int(get_average(positions) + 1)
    minimum = None
    for i in range(average):
        fuel = get_fuel(positions, i)
        if minimum is None or fuel < minimum:
            minimum = fuel

    return minimum


def part2(data):
    """ Solve Part 2"""
    positions = get_positions(data)
    max_pos = get_max(positions)

    minimum = None
    for i in range(max_pos):
        fuel = get_fuel2(positions, i)
        if minimum is None or fuel < minimum:
            minimum = fuel

    return minimum


def get_positions(data):
    """ returns crab positions """
    return list(map(int, data[0].split(',')))


def get_average(positions):
    """ returns average position of crabs """
    return sum(positions) / len(positions)


def get_max(positions):
    """ get max position """
    maximum = 0
    for position in positions:
        if position > maximum:
            maximum = position
    return maximum


def get_fuel(positions, position):
    """ get fuel consumption """
    fuel = 0
    for pos in positions:
        fuel += abs(pos - position)
    return fuel


def get_fuel2(positions, position):
    """ get fuel consumption 2 """
    fuel = 0
    for pos in positions:
        distance = abs(pos - position)
        cost = int((distance * (distance + 1)) / 2)
        fuel += cost
    return fuel


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
