def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 5
    assert part2(sample) == 12

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 4655
    assert part2(puzzle) == 20500


def part1(data):
    """ Solve Part 1"""
    lines, maximum = get_lines(data)

    matrix = []
    for _ in range(maximum):
        matrix.append([0] * maximum)

    for line in lines:
        for point in line.points():
            matrix[point.x][point.y] += 1

    count = 0
    for line in matrix:
        for value in line:
            if value > 1:
                count += 1

    # print_matrix(matrix)

    return count


def part2(data):
    """ Solve Part 2"""
    lines, maximum = get_lines(data)

    matrix = []
    for _ in range(maximum):
        matrix.append([0] * maximum)

    for line in lines:
        for point in line.points():
            matrix[point.x][point.y] += 1
        for point in line.diag_points():
            matrix[point.x][point.y] += 1

    count = 0
    for line in matrix:
        for value in line:
            if value > 1:
                count += 1

    # print_matrix(matrix)

    return count


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")"


class Line(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def points(self):
        """ yields all points on the line """
        if self.start.x == self.end.x:
            step = get_step(self.start.y, self.end.y)
            for i in range(self.start.y, self.end.y + step, step):
                yield Point(self.start.x, i)
        elif self.start.y == self.end.y:
            step = get_step(self.start.x, self.end.x)
            for i in range(self.start.x, self.end.x + step, step):
                yield Point(i, self.start.y)

    def diag_points(self):
        """ yields all points on the line """
        if (
                self.start.x - self.start.y == self.end.x - self.end.y or
                self.start.x + self.start.y == self.end.x + self.end.y
           ):
            step_x = get_step(self.start.x, self.end.x)
            step_y = get_step(self.start.y, self.end.y)
            j = self.start.y
            for i in range(self.start.x, self.end.x + step_x, step_x):
                yield Point(i, j)
                j += step_y

    def __str__(self):
        return str(self.start) + "->" + str(self.end)


def get_step(start, end):
    """ returns iteration step """
    if start <= end:
        return 1
    return -1


def get_lines(data):
    """ get hydrothermal vent lines """
    lines = []
    maximum = 0
    for line in data:
        points = line.split("->")
        start = points[0].strip().split(',')
        end = points[1].strip().split(',')
        start_x = int(start[0])
        start_y = int(start[1])
        end_x = int(end[0])
        end_y = int(end[1])

        if start_x > maximum:
            maximum = start_x
        if end_x > maximum:
            maximum = end_x

        if start_y > maximum:
            maximum = start_y
        if end_y > maximum:
            maximum = end_y

        lines.append(Line(Point(start_x, start_y), Point(end_x, end_y)))
    return lines, maximum + 1


def print_matrix(matrix):
    """ prints the matrix """
    matrix_str = ""
    for i, line in enumerate(matrix):
        for j in range(len(line)):
            if matrix[j][i] != 0:
                matrix_str += str(matrix[j][i])
            else:
                matrix_str += "."
        matrix_str += "\n"

    print(matrix_str)


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
