def main():
    """ main """
    sample = loadfile("sample.txt")
    assert part1(sample) == 26397
    assert part2(sample) == 288957

    puzzle = loadfile("input.txt")
    assert part1(puzzle) == 370407
    print(part2(puzzle))


def part1(data):
    """ Solve Part 1"""
    opened = []
    error_score = 0

    for line in data:
        opened.clear()
        for c in line:
            if c in "([{<":
                opened.append(c)
            elif c == ")":
                if opened and opened[-1] == "(":
                    opened.pop()
                else:
                    error_score += 3
                    break
            elif c == "]":
                if opened and opened[-1] == "[":
                    opened.pop()
                else:
                    error_score += 57
                    break
            elif c == "}":
                if opened and opened[-1] == "{":
                    opened.pop()
                else:
                    error_score += 1197
                    break
            elif c == ">":
                if opened and opened[-1] == "<":
                    opened.pop()
                else:
                    error_score += 25137
                    break

    return error_score


def part2(data):
    """ Solve Part 2"""
    opened = []
    scores = []

    for line in data:
        autocomplete_score = 0
        opened.clear()
        for c in line:
            if c in "([{<":
                opened.append(c)
            elif c == ")":
                if opened and opened[-1] == "(":
                    opened.pop()
                else:
                    break
            elif c == "]":
                if opened and opened[-1] == "[":
                    opened.pop()
                else:
                    break
            elif c == "}":
                if opened and opened[-1] == "{":
                    opened.pop()
                else:
                    break
            elif c == ">":
                if opened and opened[-1] == "<":
                    opened.pop()
                else:
                    break
        else:
            while opened:
                c = opened.pop()
                autocomplete_score *= 5
                if c == "(":
                    autocomplete_score += 1
                if c == "[":
                    autocomplete_score += 2
                if c == "{":
                    autocomplete_score += 3
                if c == "<":
                    autocomplete_score += 4
            scores.append(autocomplete_score)

    score = sorted(scores)[int(len(scores)/2)]

    return score


def loadfile(filename):
    """ Load file """
    data = []
    with open(filename, 'r') as file:
        data = file.readlines()
    for index, _ in enumerate(data):
        data[index] = data[index].strip()
    return data


if __name__ == "__main__":
    main()
